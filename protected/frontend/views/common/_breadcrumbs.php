<?php
use yii\widgets\Breadcrumbs;
use usni\UsniAdaptor;
use yii\helpers\Url;

$language = UsniAdaptor::app()->languageManager->selectedLanguage;
if(!empty($this->params['breadcrumbs']))
{
?>
<div class="container">
    <?php
        echo Breadcrumbs::widget(
                                [
                                    'links'                => $this->params['breadcrumbs'],
                                    'homeLink'             => [
                                                                'label' => UsniAdaptor::t('application', ($language=='en-US')?'Home':'Inicio'),
                                                                'url'   => Url::home()
                                                              ],
                                ]
                            )
    ?>
</div>
<?php
}

