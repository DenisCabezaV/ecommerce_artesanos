<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
namespace frontend\modules\site\models;

use usni\UsniAdaptor;
use frontend\modules\site\notifications\ContactEmailNotification;
use usni\library\notifications\BaseNotificationService;
/**
 * ContactForm class file.
 * 
 * @package frontend\modules\site\models
 */
class ContactForm extends \yii\base\Model
{
    /**
     * inheritdoc
     */
    public function behaviors()
    {
        return [
                    BaseNotificationService::className()  
               ];
    }
    
    /**
     * Name during contact us.
     * @var string
     */
    public $name;

    /**
     * Email during contact us.
     * @var string
     */
    public $email;
    /**
     * Store subject during contact us.
     * @var string
     */
    public $subject;
    /**
     * Store phone during contact us.
     * @var string
     */
    public $phone;
    /**
     * Store message during contact us.
     * @var string
     */
    public $message;
    /**
     * Store verifyCode during contact us.
     * @var string
     */
    public $verifyCode;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
                   [['name', 'email', 'subject', 'message', 'verifyCode'], 'required'],
                   ['email', 'email'],
                   ['phone', 'number'],
                   ['verifyCode', 'captcha', 'captchaAction' => '/site/default/captcha'],
               ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return [
                    'name'       => UsniAdaptor::t('application', ($language=='en-US')?'Name':'Nombre'),
                    'email'      => UsniAdaptor::t('users', ($language=='en-US')?'Email':'Email'),
                    'subject'    => UsniAdaptor::t('application', ($language=='en-US')?'Subject':'Asunto'),
                    'message'    => UsniAdaptor::t('application', ($language=='en-US')?'Message':'Mensaje'),
                    'phone'      => UsniAdaptor::t('application', ($language=='en-US')?'Phone':'Teléfono'),
                    'verifyCode' => UsniAdaptor::t('application', ($language=='en-US')?'Verify Code':'Verificar código'),
               ];
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [];
    }
    
    /**
     * Sends user registration email
     * @return boolean
     */
    public function sendMail()
    {
        $fromAddress                = $this->fromAddress;
        $this->emailNotification    = new ContactEmailNotification(['formModel' => $this]);
        $this->fromName             = $this->name;
        $this->fromAddress          = $this->email;
        $this->to                   = $fromAddress;
        return $this->processSend();
    }
}