<?php
/* @var $exception \yii\web\HttpException|\Exception */
/* @var $handler \yii\web\ErrorHandler */
use usni\UsniAdaptor;


$language = UsniAdaptor::app()->languageManager->selectedLanguage;
if ($exception instanceof \yii\web\HttpException) {
    $code = $exception->statusCode;
} else {
    $code = $exception->getCode();
}
$name = $handler->getExceptionName($exception);
if ($name === null) {
    $name = 'Error';
}
if ($code) {
    $name .= " (#$code)";
}



if ($exception instanceof \yii\base\UserException) 
{
    $message = $exception->getMessage();
} else {
    $message = ($language=='en-US')?'An internal server error occurred.':'Se ha producido un error interno del servidor.';
}

$this->title = $handler->htmlEncode($name);
?>
<div class="alert alert-danger">
    <h3><?= nl2br($handler->htmlEncode($message)) ?></h3>
    <p>
        <?php UsniAdaptor::t('application', ($language=='en-US')?'The above error occurred while the Web server was processing your request.':'El error anterior ocurrió mientras el servidor web estaba procesando su solicitud.'); ?>
    </p>
    <p>
        <?php UsniAdaptor::t('application', ($language=='en-US')?'Please contact us if you think this is a server error. Thank you.':'Póngase en contacto con nosotros si cree que se trata de un error del servidor. Gracias.'); ?>
    </p>
</div>