<?php

use usni\UsniAdaptor;
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
?>

<div class="row">
    <div class="col-md-8">
        <?php echo $this->render('/_contactus', ['formDTO' => $formDTO]); ?>
    </div>
    <div class="col-md-4">
        <?php echo "<h3>". UsniAdaptor::t('application', ($language=='en-US')?'Contact Details':'Datos de contacto') ."</h3>"?>
        <table class="table table-striped table-bordered">
            <tbody>
                <tr>
                    <?php
                    echo "<td>". UsniAdaptor::t('application', ($language=='en-US')?'Developed By':'Desarrollado por') ."</td>";
                    echo "<td>". UsniAdaptor::t('application', 'M y S y B') ."</td>";
                    ?>
                </tr>
                <tr>
                    <?php
                    echo "<td>". UsniAdaptor::t('application', ($language=='en-US')?'Contact Person':'Persona de contacto') ."</td>";
                    echo "<td>". UsniAdaptor::t('application', 'M y S y B') ."</td>";
                    ?>
                </tr>
                <tr>
                    <?php
                    echo "<td>". UsniAdaptor::t('application', 'Email') ."</td>";
                    echo "<td>". UsniAdaptor::t('application', 'tienda@correro.com') ."</td>";
                    ?>
                </tr>
                <tr>
                    <?php
                    echo "<td>". UsniAdaptor::t('application', ($language=='en-US')?'Phone':'Teléfono') ."</td>";
                    echo "<td>". UsniAdaptor::t('application', '+91-9818826390') ."</td>";
                    ?>
                </tr>
            </tbody>
        </table>
        <?php echo "<h3>". UsniAdaptor::t('application', ($language=='en-US')?'Address':'Dirección') ."</h3>"?>
        <p>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7017.88191962675!2d77.09889459999998!3d28.42103829999999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d220169f5e215%3A0x995dcf81cbb6d65e!2sSector+56%2C+Gurgaon%2C+Haryana!5e0!3m2!1sen!2sin!4v1466172824709" width="350" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
        </p>
    </div>
</div>