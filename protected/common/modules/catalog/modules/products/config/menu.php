<?php

use usni\library\utils\MenuUtil;
use usni\UsniAdaptor;
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$id = UsniAdaptor::app()->user->getIdentity()->id;

if (UsniAdaptor::app()->user->can('access.catalog')) {
    if ($id == 1) {

        return [
            [
                'label' => MenuUtil::wrapLabel(UsniAdaptor::t('products', ($language=='en-US')?'Products':'Productos')),
                'url' => ['/catalog/products/default/index'],
                'visible' => UsniAdaptor::app()->user->can('product.manage'),
            ],
            [
                'label' => MenuUtil::wrapLabel(UsniAdaptor::t('products', ($language=='en-US')?'Attribute Groups':'Grupos de Atributos')),
                'url' => ['/catalog/products/attribute-group/index'],
                'visible' => UsniAdaptor::app()->user->can('product.manage'),
            ],
            [
                'label' => MenuUtil::wrapLabel(UsniAdaptor::t('products', ($language=='en-US')?'Attributes':'Atributos')),
                'url' => ['/catalog/products/attribute/index'],
                'visible' => UsniAdaptor::app()->user->can('product.manage'),
            ],
            [
                'label' => MenuUtil::wrapLabel(UsniAdaptor::t('products', ($language=='en-US')?'Options':'Opciones')),
                'url' => ['/catalog/products/option/index'],
                'visible' => UsniAdaptor::app()->user->can('product.manage'),
            ],
            [
                'label' => MenuUtil::wrapLabel(UsniAdaptor::t('products', ($language=='en-US')?'Downloads':'Descargas')),
                'url' => ['/catalog/products/download/index'],
                'visible' => UsniAdaptor::app()->user->can('product.manage'),
            ],
            [
                'label' => MenuUtil::wrapLabel(UsniAdaptor::t('products', ($language=='en-US')?'Reviews':'Reseñas')),
                'url' => ['/catalog/products/review/index'],
                'visible' => UsniAdaptor::app()->user->can('productreview.manage'),
            ]
        ];
    } else {
        return [
            [
                'label' => MenuUtil::wrapLabel(UsniAdaptor::t('products', ($language=='en-US')?'Products':'Productos')),
                'url' => ['/catalog/products/default/index'],
                'visible' => UsniAdaptor::app()->user->can('product.manage'),
            ]
        ];
    }
}
return [];

