<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
namespace products\models;

use usni\library\db\TranslatableActiveRecord;
use usni\UsniAdaptor;
/**
 * ProductAttributeGroup class file.
 * 
 * @package products\models
 */
class ProductAttributeGroup extends TranslatableActiveRecord
{
    /**
     * @inheritdoc
     */
	public function rules()
	{
        return [
                    ['name',            'required'],
                    ['name',            'unique', 'targetClass' => ProductAttributeGroupTranslated::className(), 'targetAttribute' => ['name', 'language'], 'on' => 'create'],
                    ['name',            'unique', 'targetClass' => ProductAttributeGroupTranslated::className(), 'targetAttribute' => ['name', 'language'], 'filter' => ['!=', 'owner_id', $this->id], 'on' => 'update'],
                    ['name',            'string', 'max' => 128],
                    ['sort_order',      'safe'],
               ];
	}
    
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenario               = parent::scenarios();
        $scenario['update']     = $scenario['create']     = ['name', 'sort_order'];
        return $scenario;
    }
    
	/**
     * @inheritdoc
     */
    public static function getLabel($n = 1)
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        $message_1 = ($language == 'es-EC' ? 'Grupo de Atributos' : UsniAdaptor::t('products', 'Attribute Group'));
        $message_2 = ($language == 'es-EC' ? 'Grupos de Atributos' : UsniAdaptor::t('products', 'Attribute Groups'));
        return ($n == 1) ? $message_1 : $message_2;
    }
    
    /**
     * @inheritdoc
     */
	public function attributeLabels()
	{
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        $labels = [
                    'name'          => ($language == 'es-EC' ? 'Nombre de Atributo de Grupo' : UsniAdaptor::t('products', 'Attribute Group Name')),
                    'sort_order'    => ($language == 'es-EC' ? 'Orden de Clasificación' : UsniAdaptor::t('application', 'Sort Order'))
                  ];
        return parent::getTranslatedAttributeLabels($labels);
	}
    
    /**
     * @inheritdoc
     */
    public static function getTranslatableAttributes()
    {
        return ['name'];
    }
}