<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
namespace products\grid\front;

use usni\UsniAdaptor;
use usni\library\utils\Html;
use usni\fontawesome\FA;
use common\utils\ApplicationUtil;
use products\models\Product;
/**
 * CompareActionColumn class file.
 *
 * @package products\grid\front
 */
class CompareActionColumn extends \usni\library\grid\ActionColumn
{
    /**
     * @inheritdoc
     */
    protected function initDefaultButtons()
    {
        parent::initDefaultButtons();
        if (!isset($this->buttons['addToCart']))
        {
            $this->buttons['addToCart'] = array($this, 'renderAddToCartActionLink');
        }
        if (!isset($this->buttons['remove']))
        {
            $this->buttons['remove'] = array($this, 'renderRemoveActionLink');
        }
    }
    
    /**
     * Renders add to cart action link.
     * @param string $url
     * @param Model $model
     * @param string $key
     * @return string
     */
    public function renderAddToCartActionLink($url, $model, $key)
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        $icon   = FA::icon('shopping-cart');
        $cart = ApplicationUtil::getCart();
        $url    = '#';
        if ($cart->getTotalQuantity() > 0) {
            $cart_items = $cart->itemsList->toArray();
            $first = array_keys($cart_items)[0];
            $cart_product = Product::findOne(['id' => $first]);
            $cart_product_created_by = $cart_product->created_by;
            if ($cart_product_created_by != $model['created_by']) {
                return '';
            }
        }
        return Html::a($icon, $url, [
                    'title' => ($language == 'es-EC' ? 'Añadir al Carrito' : UsniAdaptor::t('cart', 'Add To Cart')),
                    'data-productid' => $model['id'],
                    'class'     => "btn btn-success btn-sm add-cart"
                ]);
    }
    
    /**
     * Renders remove action link.
     * @param string $url
     * @param Model $model
     * @param string $key
     * @return string
     */
    public function renderRemoveActionLink($url, $model, $key)
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        $icon   = FA::icon('times');
        $url    = '#';
        return Html::a($icon, $url, [
                    'title' => ($language == 'es-EC' ? 'Eliminar' : UsniAdaptor::t('products', 'Remove')),
                    'data-pjax' => '0',
                    'data-productid' => $model['id'],
                    'class'     => "btn btn-danger btn-sm productcompare-remove"
                ]);
    }
}