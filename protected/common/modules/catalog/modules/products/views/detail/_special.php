<?php
use usni\UsniAdaptor;
use usni\library\utils\DateTimeUtil;
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
?>
<table id="special-value-table" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <td class="text-left"><?php echo UsniAdaptor::t('customer', ($language=='en-US')?'Customer Group':'Grupo de clientes');?></td>
            <td class="text-right"><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Priority':'Prioridad');?></td>
            <td class="text-left"><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Price':'Precio');?></td>
            <td class="text-right"><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Start Date':'Fecha de inicio');?></td>
            <td class="text-left"><?php echo UsniAdaptor::t('products', ($language=='en-US')?'End Date':'Fecha de finalización');?></td>
            <td class="text-right"></td>
        </tr>
    </thead>
    <tbody>
        <?php 
        foreach($productSpecials as $index => $productSpecial)
        {
            ?>
        <tr>
            <td class="text-left">
                <?php echo $productSpecial['groupName']; ?>
            </td>
            <td class="text-right">
                <?php echo $productSpecial['priority']; ?>
            </td>
            <td class="text-left">
                <?php echo $productSpecial['price']; ?>
            </td>
            <td class="text-right">
                <?php echo DateTimeUtil::getFormattedDateTime($productSpecial['start_datetime']); ?>
            </td>
            <td class="text-left">
                <?php echo DateTimeUtil::getFormattedDateTime($productSpecial['end_datetime']); ?>
        </tr>
        <?php
        }
        ?>
    </tbody>
</table>

