<?php
use usni\UsniAdaptor;
use usni\library\utils\DateTimeUtil;
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
?>
<table id="discount-value-table" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <td class="text-left"><?php echo UsniAdaptor::t('customer', ($language=='en-US')?'Customer Group':'Grupo de clientes'); ?></td>
            <td class="text-right"><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Quantity':'Cantidad'); ?></td>
            <td class="text-left"><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Priority':'Prioridad'); ?></td>
            <td class="text-right"><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Price':'Precio'); ?></td>
            <td class="text-left"><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Start Date':'Fecha de inicio'); ?></td>
            <td class="text-right"><?php echo UsniAdaptor::t('products', ($language=='en-US')?'End Date':'Fecha de finalización'); ?></td>
            <td class="text-left"></td>
        </tr>
    </thead>
    <tbody>
        <?php 
        foreach($productDiscounts as $index => $productDiscount)
        {
            ?>
        <tr>
            <td class="text-left">
                <?php echo $productDiscount['groupName']; ?>
            </td>
            <td class="text-right">
                <?php echo $productDiscount['quantity']; ?>
            </td>
            <td class="text-left">
                <?php echo $productDiscount['priority']; ?>
            </td>
            <td class="text-right">
                <?php echo $productDiscount['price']; ?>
            </td>
            <td class="text-left">
                <?php echo DateTimeUtil::getFormattedDateTime($productDiscount['start_datetime']); ?>
            </td>
            <td class="text-right">
                <?php echo DateTimeUtil::getFormattedDateTime($productDiscount['end_datetime']); ?>
        </tr>
        <?php
        }
        ?>
    </tbody>
</table>

