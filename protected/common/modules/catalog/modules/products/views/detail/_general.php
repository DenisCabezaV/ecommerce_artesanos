<?php
use usni\library\widgets\DetailView;
use usni\UsniAdaptor;

/* @var $detailViewDTO \usni\library\dto\DetailViewDTO */
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$model          = $detailViewDTO->getModel();
$widgetParams   = [
                    'detailViewDTO' => $detailViewDTO,
                    'decoratorView' => false,
                    'model'         => $model,
                    'attributes'    => [
                                            'name',
                                            'alias',
                                            [
                                                'attribute'   => 'description',
                                                'format'      => 'raw'
                                            ],
                                            [
                                                'label'     => UsniAdaptor::t('products', ($language=='en-US')?'Tax Class':'Clase de impuestos'),
                                                'attribute' => 'tax_class_id',
                                                'value'     => $model['taxClassName']
                                            ],
                                            'metakeywords',
                                            'metadescription',
                                            [
                                                'label' => UsniAdaptor::t('products', ($language=='en-US')?'Product Tags':'Etiquetas de productos'),
                                                'value' => $model['tagNames']
                                            ]
                                        ]
                    ];
echo DetailView::widget($widgetParams);

