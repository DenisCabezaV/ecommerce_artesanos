<?php

use marqu3s\summernote\Summernote;
use usni\library\widgets\forms\NameWithAliasFormField;
use dosamigos\selectize\SelectizeTextInput;
use yii\helpers\Html;

/* @var $formDTO \products\dto\FormDTO */
/* @var $form \usni\library\bootstrap\ActiveForm */
$model = $formDTO->getModel();
?>
<?= $form->field($model, 'name')->widget(NameWithAliasFormField::className()); ?>
<?= $form->field($model, 'alias')->textInput(); ?>
<?= Html::activeHiddenInput($model, 'type', ['value' => 1]); ?>
<?= $form->field($model, 'description')->widget(Summernote::className()); ?>
<?= Html::activeHiddenInput($model, 'metakeywords', ['value' => '']); ?>
<?= Html::activeHiddenInput($model, 'metakeydescription', ['value' => '']); ?>
<?= $form->field($model, 'tagNames')->widget(SelectizeTextInput::className(), [
    'loadUrl' => ['default/tags'],
    'clientOptions' => [
        'plugins' => ['remove_button'],
        'valueField' => 'name',
        'labelField' => 'name',
        'searchField' => ['name'],
        'create' => true,
    ],
    'options' => ['class' => 'form-control']
]);