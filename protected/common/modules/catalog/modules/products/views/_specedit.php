<?php

use usni\library\utils\Html;

/* @var $formDTO \products\dto\FormDTO */
/* @var $form \usni\library\bootstrap\ActiveForm */
$model = $formDTO->getModel();
$sku = \products\dao\ProductDAO::getLastId() + 1;
?>
<?= $form->field($model, 'model')->textInput(); ?>
<?= Html::activeHiddenInput($model, 'sku', ['value' => $sku]); ?>
<?= $form->field($model, 'price')->textInput(); ?>
<?= $form->field($model, 'quantity')->textInput(); ?>