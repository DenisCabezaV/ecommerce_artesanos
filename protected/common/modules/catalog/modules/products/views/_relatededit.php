<?php

use products\utils\DownloadUtil;
use usni\library\utils\Html;
use yii\db\Query;
use usni\UsniAdaptor;

/* @var $formDTO \products\dto\FormDTO */
/* @var $form \usni\library\bootstrap\ActiveForm */
$model = $formDTO->getModel();

$table = UsniAdaptor::tablePrefix() . 'manufacturer';
$query = new Query();
$query->select(['id'])->from(["$table g"])->where(['like', 'name', (Yii::$app->user->identity->username)]);
$manufacturer = $query->scalar();
?>
<?= Html::activeHiddenInput($model, 'manufacturer', ['value' => $manufacturer]); ?>
<?= $form->field($model, 'categories')->select2input($formDTO->getCategories(), true, ['closeOnSelect' => false]); ?>
<?= $form->field($model, 'relatedProducts')->select2input($formDTO->getRelatedProducts(), true, ['multiple' => 'multiple'], ['closeOnSelect' => false]); ?>
<?= $form->field($model, 'download_option')->dropDownList(DownloadUtil::getOptions()); ?>
<?= $form->field($model, 'downloads')->select2input($formDTO->getDownloads(), true, ['multiple' => 'multiple'], ['closeOnSelect' => false]); ?>