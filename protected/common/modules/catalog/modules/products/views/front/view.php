<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl.html
 */
use usni\UsniAdaptor;
use yii\bootstrap\Tabs;
use products\widgets\DynamicOptionsEditView;
use products\widgets\InputRatingSubView;
use products\widgets\PriceWidget;
use common\utils\ApplicationUtil;
use products\models\Product;
$language = UsniAdaptor::app()->languageManager->selectedLanguage;

/* @var $this \products\web\ProductView */
/* @var $productDTO \products\dto\ProductDTO*/

$notSet     = ($language == 'es-EC' ? '(no disponible)' : UsniAdaptor::t('application', '(not set)'));
$customer   = UsniAdaptor::app()->user->getIdentity();

$this->params['breadcrumbs'] = ['label' => $this->product['name']];
$this->title                 = $this->product['name'];
$cart = ApplicationUtil::getCart();
$cart_product_created_by = -1;
$more_options = '';
if ($cart->getTotalQuantity() > 0) {
    $cart_items = $cart->itemsList->toArray();
    $first = array_keys($cart_items)[0];
    $cart_product = Product::findOne(['id' => $first]);
    $cart_product_created_by = $cart_product->created_by;
    if ($cart_product_created_by != $this->product['created_by']) {
        $more_options = ' disabled';
    }
}
?>
                <div class="row">
                    <div class="col-sm-8">
                        <?php echo $this->renderImages(); ?>
                        <br/>
                        <div class="row text-center">

                            <?php
                            if ($this->product['allowWishlist'])
                            {
                                ?>
                                <button type="button" data-toggle="tooltip" class="btn btn-success product-wishlist" title="" data-productid = "<?php echo $this->product['id']; ?>" data-original-title="Add to Wish List">
                                    <?php echo ($language == 'es-EC' ? 'Agregar a Lista de Deseados' : UsniAdaptor::t('products', 'Add to Wishlist')); ?>
                                </button>
                                <?php
                            }
                            ?>

                            <?php
                            if ($this->product['allowCompare'])
                            {
                                ?>
                                <button type="button" data-toggle="tooltip" class="btn btn-success add-product-compare" title="" data-productid = "<?php echo $this->product['id']; ?>" data-original-title="Compare this Product">
                                    <?php echo ($language == 'es-EC' ? 'Agregar a Comparar' : UsniAdaptor::t('products', 'Add to Compare')); ?>
                                </button>
                                <?php
                            }
                            ?>
                        </div>
                        <br/>
                        <?php
                        $items[] = [
                                        'options' => ['id' => 'tab-description'],
                                        'label' => ($language == 'es-EC' ? 'Descripción' : UsniAdaptor::t('application', 'Description')),
                                        'active' => 'true',
                                        'content' => $this->product['description'] == null ? $notSet : $this->product['description']
                                    ];
                        if (($this->product['allowReviews'] && UsniAdaptor::app()->user->isGuest === false)
                                || ($this->product['allowReviews'] && $this->product['allowGuestReviews'] && UsniAdaptor::app()->user->isGuest === true))
                        {
                            $items[] = [
                                            'options' => ['id' => 'tab-review'],
                                            'label' => ($language == 'es-EC' ? 'Reseñas' : UsniAdaptor::t('products', 'Reviews')) .
                                                "(" . $this->product['reviewCnt'] . ")",
                                            'content' => $this->render('/front/_reviewview', ['reviewListDataProvider' => $productDTO->getReviewListDataProvider()])
                                        ];
                        }
                        if(!empty($productDTO->getGroupedAttributes()))
                        {
                            $items[] = [
                                        'options' => ['id' => 'tab-specifications'],
                                        'label' => ($language == 'es-EC' ? 'Especificaciones' : UsniAdaptor::t('products', 'Specifications')),
                                        'content' => $this->render('/front/_attributes', ['data' => $productDTO->getGroupedAttributes()])
                                    ];
                        }
                        echo Tabs::widget(['items' => $items]);
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <h1><?php echo $this->product['name']; ?></h1>
                        <ul class="list-unstyled">
                            <li>
                                <span><?php echo ($language == 'es-EC' ? 'Fabricante' : UsniAdaptor::t('products', 'Manufacturer')); ?></span>:<?php echo str_repeat("&nbsp", 4) . $this->getManufacturerName(); ?>
                            </li>
                            <li>
                                <?php echo ($language == 'es-EC' ? 'Disponibilidad' : UsniAdaptor::t('products', 'Availability')) ?>
                                </span>:<?php echo str_repeat("&nbsp", 4) ?>
                                <?php echo $this->product['availability']; ?>
                            </li>
                        </ul>
                        <ul class="list-unstyled">
                            <li>
                                <?php
                                echo ($language == 'es-EC' ? 'Precio' : UsniAdaptor::t('products', 'Price')) .
                                    ': ' . PriceWidget::widget(['priceExcludingTax' => $this->product['priceExcludingTax'],
                                                             'tax'  => $this->product['tax'],
                                                             'defaultPrice' => $this->product['price'],
                                                             'isDetail' => true]);
                                ?>
                            </li>
                            <li>
                                <?php
                                if (!empty($this->product['weight']))
                                {
                                    echo ($language == 'es-EC' ? 'Peso' : UsniAdaptor::t('products', 'Weight')) .
                                        ': ' . $this->product['weight'];
                                }
                                ?>
                            </li>
                            <li>
                                <?php
                                if (!empty($this->product['dimensions']))
                                {
                                    echo $this->product['dimensions'];
                                }
                                ?>
                            </li>
                            <?php
                            if ($this->getDiscountText() != null)
                            {
                                ?>
                                <li>
                                    <hr/>
                                </li>
                                <?php
                                echo '<h3>' . ($language == 'es-EC' ? 'Precio con Descuento' : UsniAdaptor::t('products', 'Discounted Price')) . '</h3>';
                                echo $this->getDiscountText();
                            }
                            ?>
                        </ul>
                        <div id="product">
                            <hr/>
                            <form id="detailaddtocart">
                                <?php
                                $valueContent = DynamicOptionsEditView::widget(['productId' => $this->product['id'], 'assignedOptions' => $productDTO->getAssignedOptions()]);
                                if ($valueContent != null)
                                {
                                    ?>
                                    <h3><?php echo ($language == 'es-EC' ? 'Opciones Disponibles' :
                                            UsniAdaptor::t('products', 'Available Options')); ?>
                                    </h3>
                                    <?php echo $valueContent; ?>
                                    <?php
                                }
                                ?>
                                <div class="form-group">
                                    <label class="control-label" for="input-quantity">
                                        <?php echo ($language == 'es-EC' ? 'Cantidad' : UsniAdaptor::t('products', 'Quantity')); ?>
                                    </label>
                                    <input type="hidden" name="product_id" value="<?php echo $this->product['id']; ?>" />
                                    <input type="hidden" name="minimum_quantity" value="<?php echo $this->product['minimum_quantity']; ?>" id="product_minimum_quantity"/>
                                    <input type="text" name="quantity" size="5" value="1" id="product_quantity"/>&nbsp;&nbsp;
                                    <button type="button" id="button-cart" class="btn btn-success add-cart-detail" data-productid = "<?php echo $this->product['id']; ?>" <?php echo $more_options?>>
                                        <?php echo ($language == 'es-EC' ? 'Añadir al Carrito' : UsniAdaptor::t('cart', 'Add to Cart')); ?>
                                    </button>
                                    <div class="hidden" id="inputquantity-error">
                                        <?php echo ($language == 'es-EC' ? 'Cantidad de entrada debe ser >= cantidad mínima' :
                                            UsniAdaptor::t('cart', 'Input quantity should be >= minimum quantity')); ?>
                                    </div>
                                </div>
                            </form>
                            <?php
                            if ($this->product['minimum_quantity'] > 1)
                            {
                                ?>
                                <div class="alert alert-warning"><i class="fa fa-info-circle"></i>
                                    <?php echo ($language == 'es-EC' ? 'Este producto tiene una cantidad mínima de ' :
                                            UsniAdaptor::t('products', "This product has a minimum quantity of ")) .
                                        $this->product['minimum_quantity'] ?>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                        if ($customer != null)
                        {
                            ?>
                            <div class="well">
                                <?php
                                echo InputRatingSubView::widget(['product' => $this->product, 'view' => $this]);
                                ?>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="rating">
                            <?php echo $this->getReviewSummary(); ?>
                        </div>
                        <?php
                        if (!empty($productDTO->getTags()))
                        {
                            $tags = $this->renderTags($productDTO->getTags());
                            ?>
                            <div class="tags">
                                <hr/>
                                <?php echo ($language == 'es-EC' ? 'Etiquetas' : UsniAdaptor::t('products', 'Tags')) .
                                    ': ' . rtrim($tags, ","); ?>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <?php
                if ($this->product['relatedProductCount'] > 0)
                {
                    ?>
                    <h3><?php echo ($language == 'es-EC' ? 'Productos Relacionados' : UsniAdaptor::t('products', 'Related Products')); ?></h3>
                    <div class="row">
                        <?php echo $this->render('/front/_relatedproductslist', ['products' => $productDTO->getRelatedProducts()]); ?>
                    </div>
                    <?php
                }
                $this->registerScripts();