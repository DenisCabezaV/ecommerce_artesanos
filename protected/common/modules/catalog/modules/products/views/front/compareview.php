<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
use usni\UsniAdaptor;
use usni\library\grid\GridView;
use products\models\CompareProducts;
use products\grid\front\CompareActionColumn;
use products\grid\front\RatingDataColumn;
use common\utils\ApplicationUtil;
use products\models\Product;

/* @var $gridViewDTO \usni\library\dto\GridViewDTO */
/* @var $this \frontend\web\View */
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$this->title = UsniAdaptor::t('products', ($language=='en-US')?'Compare Products':'Comparar productos');
?>
<h2><?php echo $this->title;?></h2>
<?php
$this->params['breadcrumbs'] = [
                                    ['label' => UsniAdaptor::t('customer', ($language=='en-US')?'My Account':'Mi cuenta'), 'url' => UsniAdaptor::createUrl('customer/site/my-account')],
                                    ['label' => $this->title]
                                ];

$widgetParams   = [
                        'id'            => 'comparegridview',
                        'dataProvider'  => $gridViewDTO->getDataProvider(),
                        'caption'       => $this->title,
                        'layout'        => "{items}\n{summary}",
                        'tableOptions'  => ['class' => "table table-striped"],
                        'columns' => [
                            [
                                'attribute' => 'image',
                                'value'     => 'displayImage',
                                'format'    => 'raw',
                                'filter'    => false,
                                'enableSorting' => false
                            ],
                            'name',
                            'model',
                            'description',
                            [
                                'attribute' => 'manufacturer',
                                'value'     => function ($model) {
                                    $manufacturer = explode('_', $model['manufacturerName'], 2);;
                                    $manufacturer_name = count($manufacturer) == 2 ? $manufacturer[1] : $model['manufacturerName'];
                                    return $manufacturer_name;
                                }
                            ],
                            [
                                'attribute' => 'weight',
                                'value'     => 'convertedWeight',
                            ],
                            [
                                'label'     => UsniAdaptor::t('products', ($language=='en-US')?'Availability':'Disponibilidad'),
                                'value'     => 'availability',
                            ],
                            [
                                'label'     => UsniAdaptor::t('products', ($language=='en-US')?'Dimensions(L*W*H)':'Dimensiones(L*W*H)'),
                                'value'     => 'dimensions',
                            ],
                            [
                                'attribute' => 'price',
                                'value'     => 'formattedPrice'
                            ],
                            [
                                'label'     => UsniAdaptor::t('products', ($language=='en-US')?'Attributes':'Atributos'),
                                'value'     => 'attributes',
                                'format'    => 'html'
                            ],
                            [
                                'label'     => UsniAdaptor::t('products', ($language=='en-US')?'Rating':'Calificación'),
                                'class'     => RatingDataColumn::className()
                            ],
                            [
                                'class' => CompareActionColumn::className(),
                                'template' => '{addToCart} {remove}',
                                'modelClassName' => CompareProducts::className()
                            ]
                        ],
                ];
?>
<div class="compare-grid-container">
    <?php
        echo GridView::widget($widgetParams);
    ?>
</div>