<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
use usni\UsniAdaptor;
use usni\library\utils\FileUploadUtil;
use products\widgets\PriceWidget;
use usni\library\utils\Html;
use common\utils\ApplicationUtil;
use products\models\Product;
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$addCartLabel       = UsniAdaptor::t('cart', ($language=='en-US')?'Add to Cart':'Añadir al carrito');
$addWishListLabel   = UsniAdaptor::t('wishlist', ($language=='en-US')?'Add to Wish List':'Añadir a la lista de deseos');
$addCompareLabel    = UsniAdaptor::t('products', ($language=='en-US')?'Add to Compare':'Añadir a Comparar');
$cart = ApplicationUtil::getCart();
$cart_product_created_by = -1;
$more_options = '';
if ($cart->getTotalQuantity() > 0) {
    $cart_items = $cart->itemsList->toArray();
    $first = array_keys($cart_items)[0];
    $cart_product = Product::findOne(['id' => $first]);
    $cart_product_created_by = $cart_product->created_by;
    if ($cart_product_created_by != $model['created_by']) {
        $more_options = ' disabled style="cursor: not-allowed"';
    }
}
?>
<?php echo Html::beginTag('div', $containerOptions);?>
    <div class="product-thumb transition">
        <div class="image">
            <a href="<?php echo UsniAdaptor::createUrl('/catalog/products/site/detail', ['id' => $model['id']]); ?>">
                <?php echo FileUploadUtil::getThumbnailImage($model, 'image', ['thumbWidth' => $productWidth, 
                                                                                'thumbHeight' => $productHeight]); ?>
            </a>
        </div>
        <div class="caption">  
            <h4>
                <a href="<?php echo UsniAdaptor::createUrl('/catalog/products/site/detail', ['id' => $model['id']]); ?>">
                    <?php echo $model['name']; ?>
                </a>
            </h4>
            <p>
                <?php 
                        $desc = strip_tags($model['description']);
                        if(strlen($desc) > $listDescrLimit)
                        {
                            echo substr($desc, 0, $listDescrLimit) . '...';
                        }
                        else
                        {
                            echo $desc;
                        }
                    ?>
            </p>
            <p class="price"><?php echo PriceWidget::widget(['priceExcludingTax' => $model['finalPriceExcludingTax'],
                                                             'tax'  => $model['tax'],
                                                             'defaultPrice' => $model['price']]); ?></p>
        </div>
        <div class="button-group">
        <?php
                if($model['requiredOptionsCount'] == 0)
                {
                ?>

                    <input type="hidden" name="quantity" value="1" />
                        <button type="button" data-toggle="tooltip" title="<?php echo $addCartLabel;?>" class="add-cart" data-productid = "<?php echo $model['id'];?>" <?php echo $more_options?>>
                            <i class="fa fa-shopping-cart"></i>
                            <span class="hidden-xs hidden-sm hidden-md" style="font-size: 12px"><?php echo $addCartLabel;?></span>
                        </button>
                <?php
                }
                else
                {
                    $url   = UsniAdaptor::createUrl('/catalog/products/site/detail', ['id' => $model['id']]);
                ?>
                    <button type="button" data-toggle="tooltip" title="<?php echo $addCartLabel;?>" <?php echo $more_options?>>
                        <a href="<?php echo $url;?>"><i class="fa fa-shopping-cart"></i></a>
                        <a href="<?php echo $url;?>"><span class="hidden-xs hidden-sm hidden-md"><?php echo $addCartLabel;?></span></a>
                    </button>
                <?php    
                }
                ?>
                <?php
                if($allowWishList)
                {
                ?>
                    <button type="button" data-toggle="tooltip" title="<?php echo $addWishListLabel;?>" class="product-wishlist" data-productid = "<?php echo $model['id'];?>" name="<?php echo $addWishListLabel;?>">
                        <i class="fa fa-heart" id="add-to-wish-list-<?php echo $model['id'];?>"></i>
                </button>
                <?php
                }
                ?>
                
                <?php
                if($allowCompare)
                {
                ?>
                    <button type="button" data-toggle="tooltip" title="<?php echo $addCompareLabel;?>" class="add-product-compare" data-productid = "<?php echo $model['id'];?>" id="<?php echo $addCompareLabel . '-' .$model['id'];?>">
                    <i class="fa fa-exchange"></i>
                </button>
                <?php
                }
                ?>
        </div>
    </div>
<?php echo Html::endTag('div');