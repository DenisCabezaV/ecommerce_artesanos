<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */

use usni\UsniAdaptor;
use usni\library\bootstrap\ActiveForm;
use usni\library\bootstrap\FormButtons;
use usni\library\widgets\forms\NameWithAliasFormField;
use usni\library\widgets\Thumbnail;
use marqu3s\summernote\Summernote;
use usni\library\utils\StatusUtil;
use yii\helpers\Html;
use productCategories\dao\ProductCategoryDAO;

/* @var $this \usni\library\web\AdminView */
/* @var $formDTO \products\dto\ProductCategoryFormDTO */
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$model = $formDTO->getModel();
$code = 'PRD-CT-' . Yii::$app->user->id . '-' . (ProductCategoryDAO::getLastId() + 1);

if ($model->scenario == 'create') {
    $caption = UsniAdaptor::t('application', ($language=='en-US')?'Create':'Crear') . ' ' . UsniAdaptor::t('productCategories', ($language=='en-US')?'Product Category':'Categoría de productos');
} else {
    $caption = UsniAdaptor::t('application', ($language=='en-US')?'Update':'Actualizar') . ' ' . UsniAdaptor::t('productCategories', ($language=='en-US')?'Product Category':'Categoría de productos');
}
$form = ActiveForm::begin([
    'id' => 'productcategoryeditview',
    'layout' => 'horizontal',
    'caption' => $caption,
    'options' => ['enctype' => 'multipart/form-data'],
]); ?>

<?= Html::activeHiddenInput($model, 'data_category_id', ['value' => 1]); ?>
<?= $form->field($model, 'name')->widget(NameWithAliasFormField::className()); ?>
<?= $form->field($model, 'alias')->textInput(); ?>
<?= Thumbnail::widget(['model' => $model,
    'attribute' => 'image',
    'showDeleteLink' => false]); ?>
<?= $form->field($model, 'image')->fileInput(); ?>
<?= $form->field($model, 'description')->widget(Summernote::className()); ?>
<?= Html::activeHiddenInput($model, 'parent_id', ['value' => '']); ?>
<?= Html::activeHiddenInput($model, 'metakeywords', ['value' => '']); ?>
<?= Html::activeHiddenInput($model, 'metakeydescription', ['value' => '']); ?>
<?= $form->field($model, 'status')->dropDownList(StatusUtil::getDropdown()); ?>
<?= Html::activeHiddenInput($model, 'code', ['value' => $code]); ?>
<?= $form->field($model, 'displayintopmenu')->checkbox(); ?>
<?= FormButtons::widget(['cancelUrl' => UsniAdaptor::createUrl('catalog/productCategories/default/index')]); ?>

<?php ActiveForm::end(); ?>
