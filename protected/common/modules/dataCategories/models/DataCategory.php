<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl.html
 */
namespace common\modules\dataCategories\models;

use usni\UsniAdaptor;
use usni\library\db\TranslatableActiveRecord;
use usni\library\db\ActiveRecord;
use common\modules\stores\models\Store;
use yii\db\Exception;
/**
 * DataCategory class file.
 * 
 * @package common\modules\dataCategories\models
 */
class DataCategory extends TranslatableActiveRecord
{
    const ROOT_CATEGORY_ID = 1;
    
	/**
     * @inheritdoc
     */
	public function rules()
	{
		return [
                    [['name', 'status'], 'required', 'except' => 'bulkedit'],
                    ['name',             'unique', 'targetClass' => DataCategoryTranslated::className(), 'targetAttribute' => ['name', 'language'], 'on' => 'create'],
                    ['name', 'unique', 'targetClass' => DataCategoryTranslated::className(), 'targetAttribute' => ['name', 'language'], 'filter' => ['!=', 'owner_id', $this->id], 'on' => 'update'],
                    ['name',             'string', 'max' => 128],
                    ['status',           'default', 'value' => ActiveRecord::STATUS_ACTIVE],
                    ['status',         'number'],
                    [['name', 'description', 'status'], 'safe'],
                ];
	}

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $commonAttributes       = ['name', 'description', 'status'];
        $scenarios['create']    = $commonAttributes;
        $scenarios['update']    = $commonAttributes;
        $scenarios['bulkedit']  = ['status'];
        return $scenarios;
    }

	/**
     * @inheritdoc
     */
	public function attributeLabels()
	{
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
		$labels = [
                        'name'            => UsniAdaptor::t('application', ($language=='en-US')?'Name':'Nombre'),
                        'description'     => UsniAdaptor::t('application', ($language=='en-US')?'Description':'Descripción'),
                        'status'          => UsniAdaptor::t('application', ($language=='en-US')?'Status':'Estado'),
                  ];
        return parent::getTranslatedAttributeLabels($labels);
	}

	/**
     * @inheritdoc
     */
    public static function getLabel($n = 1)
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return ($n == 1) ? UsniAdaptor::t('dataCategories', ($language=='en-US')?'Data Category':'Categoría de datos') : UsniAdaptor::t('dataCategories', ($language=='en-US')?'Data Categories':'Categorías de datos');
    }
    
    /**
     * @inheritdoc
     */
    public static function getTranslatableAttributes()
    {
        return ['name', 'description'];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return array(
            'name'          => UsniAdaptor::t('applicationhint', ($language=='en-US')?'Minimum 3 characters':'Mínimo 3 caracteres'),
            'description'   => UsniAdaptor::t('applicationhint', ($language=='en-US')?'Description for the site':'Descripción del sitio'),
            'status'        => UsniAdaptor::t('datacategoryhint', ($language=='en-US')?'Status for datacategory':'Estado de la categoría de datos')
        );
    }
    
    /**
     * inheritdoc
     */
    public function beforeDelete()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        $isAllowedToDelete = $this->checkIfAllowedToDelete();
        if(!$isAllowedToDelete)
        {
            throw new Exception(($language=='en-US')?'this is root category or stores are associated to data category':'esta es la categoría raíz o las tiendas están asociadas a la categoría de datos');
        }
        return parent::beforeDelete();
    }
    
    /**
     * Check if stores associated
     * @param Model $model
     * @return boolean
     */
    public function checkIfAllowedToDelete()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        if($this->id == DataCategory::ROOT_CATEGORY_ID)
        {
            \Yii::warning(($language=='en-US')?'Root category can not be deleted from the system':'La categoría raíz no se puede borrar del sistema');
            return false;
        }
        $count = Store::find()->where('data_category_id = :dci', [':dci' => $this->id])->count();
        \Yii::info('Count of stores is ' . $count);
        if($count > 0)
        {
            \Yii::warning(($language=='en-US')?'Delete failed as stores are associated to data category':'Borrar fallido ya que las tiendas están asociadas a la categoría de datos');
            return false;
        }
        return true;
    }
}