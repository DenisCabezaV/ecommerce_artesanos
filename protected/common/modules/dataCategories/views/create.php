<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */

/* @var $this \usni\library\web\AdminView */
/* @var $formDTO \usni\library\dto\FormDTO */

use usni\UsniAdaptor;
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$this->params['breadcrumbs'] = [
        [
        'label' => UsniAdaptor::t('application', ($language=='en-US')?'Manage':'Administrar') . ' ' .
        UsniAdaptor::t('dataCategories', ($language=='en-US')?'Data Categories':'Categorías de datos'),
        'url' => ['/dataCategories/default/index']
    ],
        [
        'label' => UsniAdaptor::t('application', ($language=='en-US')?'Create':'Crear')
    ]
];
$this->title = UsniAdaptor::t('application', ($language=='en-US')?'Create':'Crear') . ' ' . UsniAdaptor::t('dataCategories', ($language=='en-US')?'Data Category':'Categorías de datos');
echo $this->render("/_form", ['model' => $formDTO->getModel()]);

