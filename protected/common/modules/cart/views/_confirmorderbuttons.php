<?php

use common\modules\order\widgets\ConfirmOrderFormButtons;
use usni\UsniAdaptor;

$language = UsniAdaptor::app()->languageManager->selectedLanguage;
echo ConfirmOrderFormButtons::widget(['submitButtonLabel' => ($language == 'es-EC' ? 'Confirmar Pedido' :
    UsniAdaptor::t('application', 'Confirm Order')),
    'cancelUrl' => UsniAdaptor::createUrl('cart/checkout/index'),
    'cancelLinkLabel' => ($language == 'es-EC' ? 'Anterior' : UsniAdaptor::t('cart', 'Back')),
    'submitButtonOptions' => ['class' => 'btn btn-success', 'id' => 'save'],
    'editCartUrl' => UsniAdaptor::createUrl('cart/default/view')
]);

