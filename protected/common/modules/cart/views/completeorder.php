<?php
use usni\UsniAdaptor;
use yii\helpers\Url;

/* @var $this \frontend\web\View */
$loggedInUser = UsniAdaptor::app()->user->getId();
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$this->title = $this->params['breadcrumbs'][] = ($language == 'es-EC' ? 'Completar Pedido' : UsniAdaptor::t('order', 'Complete Order'));
?>
<div class="well">
    <p><?php echo ($language == 'es-EC' ? 'Gracias por ordenar con nosotros. En caso de alguna duda, por favor comuníquese con nosotros' :
            UsniAdaptor::t('cart', "Thank you for placing order with us. In case of any query, please contact us back"));?>
        <?php
        if($loggedInUser != null)
        {
            $myOrdersUrl = UsniAdaptor::createUrl('customer/site/order-history');
            if ($language == 'es-EC') {
                echo "Por favor revise su pedido en la seccion <a href='" . $myOrdersUrl . "'>" . 'Mis Pedidos' . "</a>.";
            } else {
                echo "Please view your order under <a href='" . $myOrdersUrl . "'>" . UsniAdaptor::t('order', 'My Orders') . "</a> section.";
            }
        }
        ?>
    </p>
    <?php if ($language == 'es-EC') { ?>
        <p>En caso de alguna pregunta, por favor <a href="mailto:someone@example.com?Subject=Query" target="_top">envíenos un email</a>.</p>
        <div class="buttons text-right">
            <a href="<?php echo Url::home()?>" class="btn btn-success"><?php echo 'Continuar' ?></a>
        </div>
    <?php } else { ?>
        <p>In case you have any questions, please <a href="mailto:someone@example.com?Subject=Query" target="_top">send email</a>.</p>
        <div class="buttons text-right">
            <a href="<?php echo Url::home()?>" class="btn btn-success"><?php echo UsniAdaptor::t('application', 'Continue');?></a>
        </div>
    <?php } ?>
</div>
