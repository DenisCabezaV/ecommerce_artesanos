<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
use usni\UsniAdaptor;
use usni\library\bootstrap\ActiveForm;
use usni\library\bootstrap\FormButtons;
use newsletter\utils\NewsletterUtil;
use marqu3s\summernote\Summernote;

/* @var $this \usni\library\web\AdminView */
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$cancelUrl          = UsniAdaptor::createUrl('marketing/newsletter/default/index');
$model              = $formDTO->getModel();
$storeDropdownData  = $formDTO->getStoreDropdownData();
if($model->scenario == 'create')
{
    $caption = UsniAdaptor::t('application', ($language=='en-US')?'Create':'Crear') . ' ' . UsniAdaptor::t('newsletter', ($language=='en-US')?'Newsletter':'Boletín');
}
else
{
    $caption = UsniAdaptor::t('application', ($language=='en-US')?'Update':'Actualizar') . ' ' . UsniAdaptor::t('newsletter', ($language=='en-US')?'Newsletter':'Boletín');
}
$form = ActiveForm::begin([
        'id' => 'newslettereditview',
        'layout' => 'horizontal',
        'caption' => $this->title
    ]);
?>
<?= $form->field($model, 'store_id')->dropDownList($storeDropdownData);?>
<?= $form->field($model, 'to')->dropDownList(NewsletterUtil::getToNewsletterDropdown());?>
<?= $form->field($model, 'subject')->textInput(); ?>
<?= $form->field($model, 'content')->widget(Summernote::className()); ?>
<?= FormButtons::widget(['cancelUrl' => UsniAdaptor::createUrl('marketing/newsletter/default/index')]);?>
<?php ActiveForm::end(); ?>
