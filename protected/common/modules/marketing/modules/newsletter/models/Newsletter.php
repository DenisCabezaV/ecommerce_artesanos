<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
namespace newsletter\models;

use usni\library\db\TranslatableActiveRecord;
use usni\UsniAdaptor;
/**
 * Newsletter active record.
 * 
 * @package newsletter\models
 */
class Newsletter extends TranslatableActiveRecord
{   
    /**
     * Notification constants
     */
    const NOTIFY_SENDNEWSLETTER = 'sendNewsletter';
    
    /**
     * Newsletter To constant used in EditNewsletter screen dropdown.
     */
    const ALL_SUBSCRIBERS    = 1;
    const ALL_CUSTOMERS      = 2;
    
    /**
     * Send newsletter event
     */
    const EVENT_SENDNEWSLETTER = 'sendNewsletter';
    
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
                    [['subject', 'content'],                                    'required'],
                    [['store_id', 'to'],                                        'safe'],
                    [['id', 'subject', 'content', 'store_id', 'to'],  'safe'],
               ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios              = parent::scenarios();
        $scenarios['create']    = $scenarios['update'] = ['subject', 'content', 'store_id', 'to'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        $labels =   [
                        'subject'       => UsniAdaptor::t('application', ($language=='en-US')?'Subject':'Asunto'),
                        'content'       => UsniAdaptor::t('cms', ($language=='en-US')?'Content':'Contenido'),
                        'store_id'      => UsniAdaptor::t('application', ($language=='en-US')?'From':'De'),
                        'to'            => UsniAdaptor::t('application', ($language=='en-US')?'To':'Para'),
                    ];
        return parent::getTranslatedAttributeLabels($labels);
    }
    
    /**
     * @inheritdoc
     */
	public function attributeHints()
	{
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
		$hints = [
                     'subject'  => UsniAdaptor::t('newsletterhint', ($language=='en-US')?'Set subject for the newsletter.':'Fijar el tema del boletín informativo.'),
                     'store_id' => UsniAdaptor::t('newsletterhint', ($language=='en-US')?'Select store to send newsletter.':'Seleccione la tienda para enviar el boletín de noticias.'),
                 ];
        return $hints;
	}

    /**
     * @inheritdoc
     */
    public static function getLabel($n = 1)
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return ($n == 1) ? UsniAdaptor::t('newsletter', ($language=='en-US')?'Newsletter':'Boletín') : UsniAdaptor::t('newsletter', ($language=='en-US')?'Newsletters':'Boletines');
    }
    
    /**
     * @inheritdoc
     */
    public static function getTranslatableAttributes()
    {
        return ['content'];
    }
}
