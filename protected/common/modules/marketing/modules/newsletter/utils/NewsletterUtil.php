<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
namespace newsletter\utils;

use newsletter\models\Newsletter;
use usni\UsniAdaptor;
/**
 * NewsletterUtil class file.
 * 
 * @package newsletter\utils
 */
class NewsletterUtil
{   
    /**
     * Get To newsletter dropdown.
     * @return array
     */
    public static function getToNewsletterDropdown()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return [
                    Newsletter::ALL_SUBSCRIBERS  => UsniAdaptor::t('newsletter', ($language=='en-US')?'All Subscribers':'Todos los suscriptores'),
                    Newsletter::ALL_CUSTOMERS    => UsniAdaptor::t('newsletter', ($language=='en-US')?'All Customers':'Todos los clientes')
               ];
    }
}
