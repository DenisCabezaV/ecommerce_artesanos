<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
namespace common\modules\marketing\models;

use yii\base\Model;
use usni\UsniAdaptor;
/**
 * SendMailForm class file.
 * 
 * @package common\modules\marketing\models
 */
class SendMailForm extends Model
{
    /**
     * Send mail To constant used in SendMail screen dropdown.
     */
    const ALL_CUSTOMERS                 = 1;
    const CUSTOMER_GROUP                = 2;
    const CUSTOMERS                     = 3;
    const PRODUCTS                      = 4;
    
    /**
     * Notification constants
     */
    const NOTIFY_SENDMAIL = 'sendMail';
    
    /**
     * Send mail event
     */
    const EVENT_SENDMAIL = 'sendMail';
    
    /**
     * Store customer id.
     * @var array
     */
    public $customer_id = [];
    
    /**
     * Store product id.
     * @var array
     */
    public $product_id  = [];
    
    /**
     * Store group id.
     * @var array
     */
    public $group_id  = [];
    
    /**
     * Contain mail subject
     * @var string
     */
    public $subject;
    
    /**
     * Contain mail content
     * @var string 
     */
    public $content;
    
    /**
     * Contain store id
     * @var int 
     */
    public $store_id;
    
    /**
     * Contain to email address.
     * @var int 
     */
    public $to;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
                    [['subject', 'content', 'store_id'],                'required'],
                    [['customer_id', 'group_id', 'product_id'],         'safe'],
                    [['id', 'subject', 'content', 'store_id', 'to'],    'safe'],
               ];
    }
    
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios          = parent::scenarios();
        $scenarios['send']  = ['subject', 'content', 'store_id', 'to', 'customer_id', 'group_id', 'product_id'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        $labels =   [
                        'subject'       => UsniAdaptor::t('application', ($language=='en-US')?'Subject':'Asunto'),
                        'content'       => UsniAdaptor::t('cms', ($language=='en-US')?'Content':'Contenido'),
                        'store_id'      => UsniAdaptor::t('application', ($language=='en-US')?'From':'De'),
                        'to'            => UsniAdaptor::t('application', ($language=='en-US')?'To':'Para'),
                        'customer_id'   => UsniAdaptor::t('customer', ($language=='en-US')?'Customer':'Cliente'),
                        'group_id'      => UsniAdaptor::t('customer', ($language=='en-US')?'Customer Group':'Grupo de cliente'),
                        'product_id'    => UsniAdaptor::t('products', ($language=='en-US')?'Products':'Productos'),
                    ];
        return $labels;
    }
    
    /**
     * @inheritdoc
     */
	public function attributeHints()
	{
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
		$hints = [
                     'subject'  => UsniAdaptor::t('marketinghint', ($language=='en-US')?'Set subject for the mail.':'Establezca el asunto del correo.'),
                     'store_id' => UsniAdaptor::t('marketinghint', ($language=='en-US')?'Select store to send mail.':'Seleccione la tienda para enviar el correo.'),
                 ];
        return $hints;
	}

    /**
     * @inheritdoc
     */
    public static function getLabel($n = 1)
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return ($n == 1) ? UsniAdaptor::t('marketing', ($language=='en-US')?'Send Mail':'Enviar') : UsniAdaptor::t('marketing', ($language=='en-US')?'Send Mails':'Enviar');
    }
}
