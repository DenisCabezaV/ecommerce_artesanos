<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
namespace common\modules\order\models;

use usni\library\db\ActiveRecord;
use usni\UsniAdaptor;
use products\models\Product;
/**
 * OrderProduct active record.
 * 
 * @package common\modules\Order\models
 */
class OrderProduct extends ActiveRecord 
{
    /**
     * This would be use on order product detaiview screen.
     * @var string 
     */
    public $orderProductInformation;
    
	/**
     * @inheritdoc
     */
	public function rules()
	{
		return [
                    [['product_id', 'order_id', 'item_code'],                            'required'],
                    [['order_id', 'product_id', 'quantity', 'options', 'displayed_options', 'item_code', 'options_price'],    'safe'],
               ];
	}
    
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = $scenarios['update'] = ['order_id', 'product_id', 'quantity', 'options', 'displayed_options', 'options_price'];
        return $scenarios;
    }

	/**
     * @inheritdoc
     */
	public function attributeLabels()
	{
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
		$labels = [
                     'order_id'     => UsniAdaptor::t('order', ($language=='en-US')?'Order':'Pedido'),
                     'product_id'	=> Product::getLabel(1),
                     'quantity'     => UsniAdaptor::t('products', ($language=='en-US')?'Quantity':'Cantidad'),
                     'name'         => UsniAdaptor::t('application', ($language=='en-US')?'Name':'Nombre'),
                     'model'        => UsniAdaptor::t('products', ($language=='en-US')?'Model':'Modelo'),
                     'price'        => UsniAdaptor::t('products', ($language=='en-US')?'Price':'Precio'),
                     'total'        => UsniAdaptor::t('application', ($language=='en-US')?'Total':'Total'),
                     'tax'          => UsniAdaptor::t('tax', ($language=='en-US')?'Tax':'Impuesto'),
                     'options_price'=> UsniAdaptor::t('products', ($language=='en-US')?'Options Price':'Opciones de precios')
                  ];
        return parent::getTranslatedAttributeLabels($labels);
	}
    
    /**
     * @inheritdoc
     */
    public static function getLabel($n = 1)
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return ($n == 1) ? UsniAdaptor::t('order', ($language=='en-US')?'Order Product':'Pedido del producto') : UsniAdaptor::t('order', ($language=='en-US')?'Order Products':'Pedido de productos');
    }
}