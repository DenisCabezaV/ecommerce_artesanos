<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
namespace common\modules\order\models;

use usni\UsniAdaptor;
/**
 * OrderPaymentTransactionMap active record.
 * 
 * @package common\modules\order\models
 */
class OrderPaymentTransactionMap extends \usni\library\db\ActiveRecord 
{   
    /**
     * @inheritdoc
     */
	public function rules()
	{
		return [
                    [['order_id', 'amount', 'transaction_record_id', 'payment_method'],  'required'],
                    [['order_id', 'amount', 'transaction_record_id', 'payment_method'],  'safe'],
               ];
	}
    
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = $scenarios['update'] = ['order_id', 'amount', 'transaction_record_id', 'payment_method'];
        return $scenarios;
    }

	/**
     * @inheritdoc
     */
	public function attributeLabels()
	{
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
		$labels = [
                     'order_id'         => UsniAdaptor::t('order', ($language=='en-US')?'Order':'Pedido'),
                     'amount'           => UsniAdaptor::t('order', ($language=='en-US')?'Amount':'Cantidad'),
                     'transaction_record_id'    => UsniAdaptor::t('order', ($language=='en-US')?'Transaction Record':'Registro de transacciones'),
                     'payment_method'   => UsniAdaptor::t('payment', ($language=='en-US')?'Payment Method':'Forma de pago')
                  ];
        return $labels;
	}
    
    /**
     * @inheritdoc
     */
    public static function getLabel($n = 1)
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return UsniAdaptor::t('order', ($language=='en-US')?'Order Payment Transaction Map':'Mapa de transacciones de pago de pedidos');
    }
}