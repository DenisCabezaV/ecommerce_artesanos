<?php
namespace common\modules\order\models;

use usni\library\db\TranslatableActiveRecord;
use usni\UsniAdaptor;
use common\modules\order\models\Order;
/**
 * Invoice active record.
 * 
 * @package common\modules\Order\models
 */
class Invoice extends TranslatableActiveRecord 
{   
    /**
     * @inheritdoc
     */
	public function rules()
	{
		return [
                    [['unique_id', 'order_id', 'price_excluding_tax', 'tax', 'shipping_fee', 'total_items'], 'required'],
                    [['id', 'unique_id', 'order_id', 'price_excluding_tax', 'tax', 'shipping_fee', 'total_items', 'terms'],   'safe'],
               ];
	}
    
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = $scenarios['update'] = ['unique_id', 'order_id', 'price_excluding_tax', 'tax', 'shipping_fee', 'total_items', 'terms'];
        return $scenarios;
    }

	/**
     * @inheritdoc
     */
	public function attributeLabels()
	{
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
		$labels = [
                     'order_id'             => UsniAdaptor::t('order', ($language=='en-US')?'Order':'Pedido'),
                     'unique_id'            => UsniAdaptor::t('order', ($language=='en-US')?'Unique Id':'Id. único'),
                     'price_excluding_tax'  => UsniAdaptor::t('products', ($language=='en-US')?'Price Excluding Tax':'Precio sin impuesto'),
                     'tax'                  => UsniAdaptor::t('tax', ($language=='en-US')?'Tax':'Impuestos'),
                     'total_items'          => UsniAdaptor::t('order', ($language=='en-US')?'Total Items':'Total de artículos'),
                     'shipping_fee'         => UsniAdaptor::t('shipping', ($language=='en-US')?'Shipping Fee':'Costo de envío'),
                     'terms'                => UsniAdaptor::t('order', ($language=='en-US')?'Terms':'Términos'),
                  ];
        return parent::getTranslatedAttributeLabels($labels);
	}
    
    /**
     * @inheritdoc
     */
    public static function getLabel($n = 1)
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return ($n == 1) ? UsniAdaptor::t('order', ($language=='en-US')?'Invoice':'Factura') : UsniAdaptor::t('order', ($language=='en-US')?'Invoices':'Facturas');
    }
    
    /**
     * @inheritdoc
     */
    public static function getTranslatableAttributes()
    {
        return ['terms'];
    }
    
    /**
     * Get order for the invoice
     * @return ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}