<?php
use usni\UsniAdaptor;
use usni\library\utils\Html;
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
?>

<?php echo Html::tag('strong', UsniAdaptor::t('order', ($language=='en-US')?'Invoice Number:':'Número de factura:')) .  ' ' . $invoiceNo?><br/>
<?php echo Html::tag('strong', UsniAdaptor::t('order', ($language=='en-US')?'Order Number:':'Número de pedido:')) . ' ' . $orderNo?><br/>
<?php echo Html::tag('strong', UsniAdaptor::t('order', ($language=='en-US')?'Issue Date:':'Fecha de emisión:')) . ' ' . $issueDate?><br/>
<?php echo Html::tag('strong', UsniAdaptor::t('order', ($language=='en-US')?'Payment Method:':'Método de pago:')) . ' ' . $paymentMethod?><br/>
