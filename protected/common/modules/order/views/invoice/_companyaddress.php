<?php
use usni\UsniAdaptor;
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
?>
 
<address>
    <strong><?php echo UsniAdaptor::t('stores', $storeName); ?></strong><br/>
    <?php echo $country; ?>
</address>
<?php echo UsniAdaptor::t('users', ($language=='en-US')?'Mobile':'Movil') . ': ' . $mobile ?> <br/>
<?php echo UsniAdaptor::t('users', ($language=='en-US')?'Email':'E-mail') . ': ' . $email ?> <br/>
