<?php
use usni\UsniAdaptor;
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
?>
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th><?php echo UsniAdaptor::t('products', 'SKU'); ?></th>
            <th><?php echo UsniAdaptor::t('application', ($language=='en-US')?'Name':'Nombre'); ?></th>
            <th><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Options':'Opciones') ?></th>
            <th><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Quantity':'Cantidad'); ?></th>
            <th><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Price':'Precio'); ?></th>
            <th><?php echo UsniAdaptor::t('tax', ($language=='en-US')?'Tax':'Impuesto');    ?></th>
            <th><?php echo UsniAdaptor::t('application', ($language=='en-US')?'Total':'Total'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($orderProducts as $orderProduct)
        {
            ?>
            <tr>
                <td><?php echo $orderProduct['sku']; ?></td>
                <td><?php echo $orderProduct['name']; ?></td>
                <td><?php echo $orderProduct['displayed_options'];?></td>
                <td><?php echo $orderProduct['quantity']; ?></td>
                <td><?php echo $this->getPriceWithSymbol($orderProduct['price'], $currencySymbol); ?></td>
                <td><?php echo $this->getPriceWithSymbol($orderProduct['tax'], $currencySymbol); ?></td>
                <td><?php echo $this->getPriceWithSymbol($orderProduct['total'], $currencySymbol); ?></td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>