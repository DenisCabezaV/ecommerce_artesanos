<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
use usni\UsniAdaptor;
use usni\library\grid\GridView;
use products\models\Product;
use common\modules\order\grid\FormattedPriceColumn;
use common\modules\order\grid\OrderStatusDataColumn;
use common\modules\order\models\OrderProduct;
use usni\library\dataproviders\ArrayRecordDataProvider;

/* @var $gridViewDTO \usni\library\dto\GridViewDTO */
/* @var $this \usni\library\web\AdminView */
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$title          = UsniAdaptor::t('order', ($language=='en-US')?'Latest Orders':'Últimos pedidos');
$user_id = UsniAdaptor::app()->getUser()->id;
$initial_data_provider = $gridViewDTO->getDataProvider();
$initial_models = $initial_data_provider->getModels();
$models = array();
foreach ($initial_models as $model) {
    $one_product_id = OrderProduct::findOne(['order_id' => $model['id']])->product_id;
    $created_id = Product::findOne(['id' => $one_product_id])->created_by;
    if ($user_id==1 || $user_id==$created_id) {
        $models[] = $model;
    }
}
$data_provider = new ArrayRecordDataProvider();
$data_provider = $initial_data_provider;
$data_provider->setModels($models);
$gridViewDTO->setDataProvider($data_provider);
$widgetParams   = [
                        'id'            => 'latestordersgridview',
                        'dataProvider'  => $gridViewDTO->getDataProvider(),
                        'caption'       => $title,
                        'modelClass'    => Product::className(),
                        'layout'        => "<div class='panel panel-default'>
                                                <div class='panel-heading'>{caption}</div>
                                                    {items}
                                               </div>",
                        'columns'       => [
                                                [
                                                    'label'     => ($language=='en-US')?'Id Unique':'Id. Único',
                                                    'attribute' => 'unique_id'
                                                ],
                                                [
                                                    'label'     => ($language=='en-US')?'Name':'Nombre',
                                                    'attribute' => 'name'
                                                ],
                                                [
                                                    'label'     => UsniAdaptor::t('products', ($language=='en-US')?'Amount':'Cantidad'),
                                                    'attribute' => 'amount',
                                                    'class'     => FormattedPriceColumn::className()
                                                ],
                                                [
                                                    'label'     => ($language=='en-US')?'Status':'Estado',
                                                    'attribute' => 'status',
                                                    'class' => OrderStatusDataColumn::className()
                                                ],
                                        ],
                ];
echo GridView::widget($widgetParams);