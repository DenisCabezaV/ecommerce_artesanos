<?php
use usni\UsniAdaptor;
/* @var $this \usni\library\web\AdminView */
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
?>
<div class="table-responsive" id="shopping-cart-admin-full">
    <table class="table table-bordered">
        <thead>
            <tr>
                <td><?php echo UsniAdaptor::t('application', ($language=='en-US')?'Name':'Nombre') ?></td>
                <!--<td><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Model':'Modelo') ?></td>
                <td><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Options':'Opciones') ?></td>-->
                <td><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Quantity':'Cantidad') ?></td>
                <td><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Unit Price':'Precio unitario') ?></td>
                <!--<td><?php echo UsniAdaptor::t('tax', ($language=='en-US')?'Tax':'Impuesto') ?></td>-->
                <td><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Total Price':'Precio Total') ?></td>
            </tr>
        </thead>
        <tbody>
            <?php
            echo $items;
            ?>
        </tbody>
    </table>
</div>
<br/>
<div class="row">
    <div class="col-sm-4 col-sm-offset-8">
        <table class="table table-bordered">
            <tbody>
                <!--<tr>
                    <td class="text-right"><strong><?php echo UsniAdaptor::t('products', ($language=='en-US')?'Sub-Total':'Subtotal'); ?></strong></td>
                    <td class="text-right"><?php echo $this->getPriceWithSymbol($totalUnitPrice, $currencySymbol); ?></td>
                </tr>
                <tr>
                    <td class="text-right"><strong><?php echo UsniAdaptor::t('tax', ($language=='en-US')?'Tax':'Impuesto'); ?></strong></td>
                    <td class="text-right"><?php echo $this->getPriceWithSymbol($totalTax, $currencySymbol); ?></td>
                </tr>-->
                <?php
                if ($shippingPrice > 0)
                {
                    ?>
                    <tr>
                        <td class="text-right"><strong><?php echo UsniAdaptor::t('shipping', ($language=='en-US')?'Shipping Cost':'Costo de envío'); ?></strong></td>
                        <td class="text-right"><?php echo $this->getPriceWithSymbol($shippingPrice, $currencySymbol); ?></td>
                    </tr>
                    <?php
                }
                ?>
                <tr>
                    <td class="text-right"><strong><?php echo UsniAdaptor::t('application', ($language=='en-US')?'Total':'Total'); ?></strong></td>
                    <td class="text-right"><?php echo $this->getPriceWithSymbol($totalPrice, $currencySymbol); ?></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<br/>