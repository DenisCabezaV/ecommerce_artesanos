<?php
use common\utils\ApplicationUtil;
use usni\UsniAdaptor;
use usni\library\bootstrap\ActiveForm;
use common\modules\order\widgets\AdminConfirmCartSubView;
use common\modules\order\widgets\ConfirmOrderFormButtons;

/* @var $this \frontend\web\View */
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$this->params['breadcrumbs'][]  = [
                                    'label' => UsniAdaptor::t('orders', ($language=='en-US')?'Manage Orders':'Administrar pedidos'),
                                    'url'   => UsniAdaptor::createUrl('order/default/index')
                                  ];
$this->title    = $this->params['breadcrumbs'][] = UsniAdaptor::t('cart', ($language=='en-US')?'Confirm Order':'Confirmar pedido');
$order          = ApplicationUtil::getCheckoutFormModel('order');
$cart           = ApplicationUtil::getCart();

$form   = ActiveForm::begin([
                                'id'     => 'reviewview', 
                                'layout' => 'horizontal',
                                'caption'=> $this->title
                            ]); ?>

        <div class="row">
            <div class="col-sm-3">
                <legend><?php echo UsniAdaptor::t('customer', ($language=='en-US')?'Billing Address':'Dirección de facturación');?></legend>
                <?php echo $reviewDTO->getBillingContent();?>
            </div>
            <?php
            if($cart->isShippingRequired())
            {
            ?>
                <div class="col-sm-3">
                    <legend><?php echo UsniAdaptor::t('customer', ($language=='en-US')?'Shipping Address':'Dirección de envío');?></legend>
                    <?php echo $reviewDTO->getShippingContent();?>
                </div>
                <div class="col-sm-3">
                    <legend><?php echo UsniAdaptor::t('shipping', ($language=='en-US')?'Shipping Method':'Forma de envío');?></legend>
                    <?php echo $reviewDTO->getShippingName();?>
                </div>
            <?php
            }
            ?>
            <div class="col-sm-3">
                <legend><?php echo UsniAdaptor::t('payment', ($language=='en-US')?'Payment Method':'Forma de pago');?></legend>
                <?php echo $reviewDTO->getPaymentMethodName();?>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-sm-12">
                <?php echo AdminConfirmCartSubView::widget();?>
            </div>
        </div>
        <?php
            echo $form->field($model, 'comments')->textarea();
            echo $form->field($model, 'status')->select2Input($reviewDTO->getAllStatus());
        ?>
    <?= ConfirmOrderFormButtons::widget(['submitButtonLabel' => UsniAdaptor::t('application', ($language=='en-US')?'Confirm Order':'Confirmar pedido'),
                             'cancelUrl' => UsniAdaptor::createUrl('order/default/checkout'),
                             'cancelLinkLabel' => UsniAdaptor::t('cart', 'Back'),
                             'editCartUrl' => UsniAdaptor::createUrl('order/default/add-to-cart')
                             ]);?>
<?php ActiveForm::end();