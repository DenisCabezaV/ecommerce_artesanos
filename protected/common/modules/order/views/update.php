<?php
/* @var $this \usni\library\web\AdminView */
/* @var $formDTO \common\modules\order\dto\AdminCheckoutDTO */

use usni\UsniAdaptor;
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$this->params['breadcrumbs'] = [
        [
        'label' => UsniAdaptor::t('application', ($language=='en-US')?'Manage':'Administrar') . ' ' .
        UsniAdaptor::t('order', ($language=='en-US')?'Orders':'Pedidos'),
        'url' => ['/order/default/index']
    ],
        [
        'label' => UsniAdaptor::t('application', ($language=='en-US')?'Update':'Actualizar') . " " . UsniAdaptor::t('order', ($language=='en-US')?'Order':'Pedido')
    ]
];
$this->title = UsniAdaptor::t('application', ($language=='en-US')?'Update':'Actualizar') . ' ' . UsniAdaptor::t('order', ($language=='en-US')?'Order':'Pedido') . ' #' . $formDTO->getCheckout()->order->id;
echo $this->render("/_orderedit", ['formDTO' => $formDTO, 'caption' => $this->title]);