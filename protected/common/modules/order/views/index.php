<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
use usni\UsniAdaptor;
use usni\library\grid\GridView;
use usni\library\grid\ActionToolbar;
use yii\grid\CheckboxColumn;
use common\modules\order\grid\OrderStatusDataColumn;
use common\modules\order\models\Order;
use common\modules\order\grid\OrderActionColumn;
use products\models\Product;
use common\modules\order\models\OrderProduct;
use usni\library\dataproviders\ArrayRecordDataProvider;

/* @var $gridViewDTO \common\modules\order\dto\GridViewDTO */
/* @var $this \usni\library\web\AdminView */
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$title          = UsniAdaptor::t('order', ($language=='en-US')?'Manage Orders':'Administrar pedidos');
$this->title    = $this->params['breadcrumbs'][] = $title;

$toolbarParams  = [
    'createUrl'     => 'create',
    'showBulkEdit'  => false,
    'showBulkDelete'=> false,
    'gridId'        => 'ordergridview',
    'pjaxId'        => 'ordergridview-pjax',
    'permissionPrefix'  => 'order',
    'showModalDetail' => false
];
$user_id = UsniAdaptor::app()->getUser()->id;
$initial_data_provider = $gridViewDTO->getDataProvider();
$initial_models = $initial_data_provider->getModels();
$models = array();
foreach ($initial_models as $model) {
    $one_product_id = OrderProduct::findOne(['order_id' => $model['id']])->product_id;
    $created_id = Product::findOne(['id' => $one_product_id])->created_by;
    if ($user_id==1 || $user_id==$created_id) {
        $models[] = $model;
    }
}
$data_provider = new ArrayRecordDataProvider();
$data_provider = $initial_data_provider;
$data_provider->setModels($models);
$gridViewDTO->setDataProvider($data_provider);
$widgetParams   = [
                        'id'            => 'ordergridview',
                        'dataProvider'  => $gridViewDTO->getDataProvider(),
                        'filterModel'   => $gridViewDTO->getSearchModel(),
                        'caption'       => $title,
                        'modelClass'    => Order::className(),
                        'columns' => [
                            ['class' => CheckboxColumn::className()],
                            'unique_id',
                            'name',
                            [
                                'label'         => UsniAdaptor::t('customer', ($language=='en-US')?'Customer':'Cliente'),
                                'attribute'     => 'customer_id',
                                'value'         => 'username',
                                'filter'        => $gridViewDTO->getCustomerFilterData()
                            ],
                            [
                                'attribute'     => 'status',
                                'class'         => OrderStatusDataColumn::className(),
                                'filter'        => $gridViewDTO->getStatusData()
                            ],
                            [
                                'label'         => UsniAdaptor::t('order', ($language=='en-US')?'Amount':'Cantidad'),
                                'attribute'     => 'amount'
                            ],
                            [
                                'label'         => UsniAdaptor::t('shipping', ($language=='en-US')?'Shipping Method':'Forma de envío'),
                                'attribute'     => 'shipping',
                                'value'         => 'shipping_method_name',
                                'filter'        => $gridViewDTO->getShippingMethods()
                            ],
                            [
                                'label'         => UsniAdaptor::t('payment', ($language=='en-US')?'Payment Method':'Forma de pago'),
                                'attribute'     => 'payment_method',
                                'value'         => 'payment_method_name',
                                'filter'        => $gridViewDTO->getPaymentMethods()
                            ],
                            [
                                'class'         => OrderActionColumn::className(),
                                'template'      => '{view} {invoice}',
                                'modelClassName' => Order::className()
                            ]
                        ],
                ];
echo ActionToolbar::widget($toolbarParams);
echo GridView::widget($widgetParams);