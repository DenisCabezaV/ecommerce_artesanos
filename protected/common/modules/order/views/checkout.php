<?php
use usni\UsniAdaptor;
use usni\library\bootstrap\ActiveForm;
use usni\library\bootstrap\FormButtons;

/* @var $this \usni\library\web\AdminView */
/* @var $formDTO \cart\dto\CheckoutDTO */
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$isShippingRequired = $formDTO->getCart()->isShippingRequired();

$this->params['breadcrumbs'][] = [
                                    'label' => UsniAdaptor::t('application', ($language=='en-US')?'Manage':'Administrar') . ' ' .
                                    UsniAdaptor::t('order', ($language=='en-US')?'Orders':'Pedidos'),
                                    'url' => ['/order/default/index']
                                ];
$this->title = $this->params['breadcrumbs'][] = UsniAdaptor::t('cart', ($language=='en-US')?'Checkout':'Realizar Pedido');

$shippingMethods = $formDTO->getShippingMethods();
$paymentMethods  = $formDTO->getPaymentMethods();
if($isShippingRequired && empty($shippingMethods))
{
?>
    <div class="well">
        <p><?php echo UsniAdaptor::t('cart', ($language=='en-US')?"Shipping methods are not enabled. Please contact admin to enable shipping methods.":"Los métodos de envío no están habilitados. Por favor, póngase en contacto con admin para habilitar los métodos de envío.");?></p>
    </div>
<?php
}
elseif(empty($paymentMethods))
{
  ?>
    <div class="well">
        <p><?php echo UsniAdaptor::t('cart', ($language=='en-US')?"Payment methods are not enabled. Please contact admin to enable payment methods.":"Los métodos de pago no están habilitados. Por favor, póngase en contacto con admin para habilitar los métodos de pago.");?></p>
    </div>
  <?php
}
else
{
?>
<?php $form = ActiveForm::begin([
                                    'id'          => 'checkoutview',
                                    'caption'     => $this->title,
                                    'fieldConfig' => [ 
                                                        'template' => "{beginLabel}{labelTitle}{endLabel}{beginWrapper}{input}{error}{endWrapper}",
                                                        'horizontalCssClasses' => [
                                                                                        'label'     => '',
                                                                                        'offset'    => '',
                                                                                        'wrapper'   => '',
                                                                                        'error'     => '',
                                                                                        'hint'      => '',
                                                                                   ]
                                                    ]
                               ]); ?>
<div class="row">
    <div class="col-sm-6">
        <legend><span class="badge">1</span> <?php echo UsniAdaptor::t('customer', ($language=='en-US')?'Billing Address':'Dirección de facturación');?></legend>
        <?php echo $this->render('@cart/views/_billingedit', ['form' => $form, 'model' => $formDTO->getCheckout()->billingInfoEditForm]);?>
    </div>
    <?php
    if($isShippingRequired)
    {
    ?>
        <div class="col-sm-6">
            <legend><span class="badge">2</span> <?php echo UsniAdaptor::t('customer', ($language=='en-US')?'Shipping Address':'Dirección de envío');?></legend>
            <?php echo $this->render('@cart/views/_shippingedit', ['form' => $form, 'model' => $formDTO->getCheckout()->deliveryInfoEditForm]);?>
        </div>
        <div class="col-sm-6">
            <legend><span class="badge">3</span> <?php echo UsniAdaptor::t('shipping', ($language=='en-US')?'Shipping Method':'Forma de envío');?></legend>
            <?php echo $this->render('@cart/views/_deliveryoptions', ['form' => $form, 'formDTO' => $formDTO]);?>
        </div>
        <div class="col-sm-6">
            <legend><span class="badge">4</span> <?php echo UsniAdaptor::t('payment', ($language=='en-US')?'Payment Method':'Forma de pago');?></legend>
            <?php echo $this->render('@cart/views/_paymentoptions', ['form' => $form, 'formDTO' => $formDTO]);?>
        </div>
    <?php
    }
    else
    {
    ?>
        <div class="col-sm-6">
            <legend><span class="badge">2</span> <?php echo UsniAdaptor::t('payment', ($language=='en-US')?'Payment Method':'Forma de pago');?></legend>
            <?php echo $this->render('@cart/views/_paymentoptions', ['form' => $form, 'formDTO' => $formDTO]);?>
        </div>
    <?php
    }
    ?>
</div>
<?= FormButtons::widget(['submitButtonLabel' => UsniAdaptor::t('application', ($language=='en-US')?'Continue':'Continuar'),
                         'cancelUrl' => UsniAdaptor::createUrl('order/default/add-to-cart'),
                         'cancelLinkLabel' => UsniAdaptor::t('order', ($language=='en-US')?'Previous':'Anterior')
                         ]);?>

<?php ActiveForm::end();
}