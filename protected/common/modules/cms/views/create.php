<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
/* @var $this \usni\library\web\AdminView */
/* @var $formDTO \common\modules\cms\dto\FormDTO */

use usni\UsniAdaptor;
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$this->params['breadcrumbs'] = [
        [
        'label' => ($language == 'es-EC' ? 'Administrar' : UsniAdaptor::t('application', 'Manage')) . ' ' .
            ($language == 'es-EC' ? 'Páginas' : UsniAdaptor::t('cms', 'Pages')),
        'url' => ['/cms/page/index']
    ],
        [
        'label' => ($language == 'es-EC' ? 'Crear' : UsniAdaptor::t('application', 'Create'))
    ]
];
$this->title = ($language == 'es-EC' ? 'Crear' : UsniAdaptor::t('application', 'Create')) .
    ' ' . ($language == 'es-EC' ? 'Página' : UsniAdaptor::t('cms', 'Page'));
echo $this->render("/_form", ['formDTO' => $formDTO]);

