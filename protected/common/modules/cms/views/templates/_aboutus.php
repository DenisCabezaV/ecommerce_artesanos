<p>
La Escuela de Comercio Exterior y Negocios Internacionales de la Pontificia Universidad Católica del Ecuador Sede Esmeraldas, entre estudiantes y docentes han trabajado en el proyecto de vinculación con nombre: Internacionalización de las mipymes del sector artesanal del cantón Atacames. El objetivo del proyecto es Fortalecer las capacidades de las Mipymes del sector artesanal del cantón Atacames para la internacionalización de sus productos: 
</p>
<p>
Algunas de las actividades fueron:
</p>
<ul>
<li>Implementación de imagen corporativa para los artesanos del cantón Atacames. Diseño empaque Stand, camisas  </li>
<li>Diseño de sitio web para la promoción de las artesanías Atacames   </li>
<li>Importar herramientas para la actividad de los artesanos para su posterior donación Fase  </li>
<li>Gestión de financiamiento para promoción internacional de artesanías mediante portal web </li>
<li>Análisis de mercados de artesanías fabricadas y Promoción en sitios web </li>
<li>Envió de muestras de artesanías al exterior por medio de los artesanos </li>
</ul>
