<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
use usni\UsniAdaptor;
use usni\library\widgets\BrowseDropdown;

/* @var $formDTO \common\modules\cms\dto\FormDTO */

$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$model  = $formDTO->getModel();
$this->params['breadcrumbs'] = [
        [
        'label' => ($language == 'es-EC' ? 'Administrar' : UsniAdaptor::t('application', 'Manage')) . ' ' .
            ($language == 'es-EC' ? 'Páginas' : UsniAdaptor::t('cms', 'Pages')),
        'url' => ['/cms/page/index']
    ],
        [
        'label' => ($language == 'es-EC' ? 'Actualizar' : UsniAdaptor::t('application', 'Update')) . ' #' . $model->id
    ]
];

$browseParams   = ['permission' => 'page.updateother',
                   'data'   => $formDTO->getBrowseModels(),
                   'model'  => $model];
echo BrowseDropdown::widget($browseParams);

$this->title = ($language == 'es-EC' ? 'Actualizar' : UsniAdaptor::t('application', 'Update')) .
    ' ' . ($language == 'es-EC' ? 'Página' : UsniAdaptor::t('cms', 'Page'));
echo $this->render("/_form", ['formDTO' => $formDTO]);