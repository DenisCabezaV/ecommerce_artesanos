<?php
use usni\UsniAdaptor;
use common\modules\extension\models\Extension;
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
return [
        'name'      => UsniAdaptor::t('shipping', ($language=='en-US')?'Free Shipping':'Envío gratuito'),
        'code'      => 'free',
        'author'    => 'WhatACart',
        'version'   => '1.0',
        'product_version' => '2.0.0',
        'status'    => Extension::STATUS_INACTIVE,
        'category'  => 'shipping'
    ];