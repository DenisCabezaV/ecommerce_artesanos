<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
use usni\UsniAdaptor;

/* @var $listViewDTO \common\modules\manufacturer\dto\ManufacturerListViewDTO */
/* @var $this \frontend\web\View */

$this->params['manList'] = $listViewDTO->getManList();
$this->leftnavView  = '/front/_sidebar';
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$manufacturer       = $listViewDTO->getManufacturer();
$title              = UsniAdaptor::t('application', ($language=='en-US')?'Search Results':'Resultados de la búsqueda');
$this->title        = $this->params['breadcrumbs'][] = $title;
$manufacturer_aux = explode('_', $manufacturer['name'], 2);;
$manufacturer_name = count($manufacturer_aux) == 2 ? $manufacturer_aux[1] : $manufacturer['name'];
?>
<h2><?php echo $title . '-' . $manufacturer_name?></h2>
<?php
echo $this->render('//common/_searchresults', ['listViewDTO' => $listViewDTO]);