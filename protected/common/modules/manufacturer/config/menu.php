<?php
use usni\library\utils\MenuUtil;
use usni\UsniAdaptor;
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
if(UsniAdaptor::app()->user->can('access.manufacturer'))
{
    return [    
                [
                    'label'       => MenuUtil::wrapLabel(UsniAdaptor::t('manufacturer',($language=='en-US')? 'Manufacturers':'Fabricantes')),
                    'url'         => ['/manufacturer/default/index'],
                    'itemOptions' => ['class' => 'navblock-header']
                ]
            ];
}
return [];

