<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
namespace taxes\db;

use usni\library\db\DataManager;
use taxes\models\ProductTaxClass;
use usni\UsniAdaptor;
/**
 * Loads default data related to product tax class.
 * 
 * @package taxes\db
 */
class ProductTaxClassDataManager extends DataManager
{   
    /**
     * @inheritdoc
     */
    public static function getModelClassName()
    {
        return ProductTaxClass::className();
    }

    /**
     * @inheritdoc
     */
    public function getDefaultDataSet()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
         return [
                    [
                        'name'          => UsniAdaptor::t('tax', ($language=='en-US')?'taxable goods':'Bienes tributario'),
                        'description'   => UsniAdaptor::t('tax', ($language=='en-US')?'Applied to goods on which tax has to be applied':'Se aplica a los bienes sobre los que debe aplicarse el impuesto')
                    ]
                ];
    }
}