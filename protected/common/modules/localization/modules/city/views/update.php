<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
use usni\UsniAdaptor;

use usni\library\widgets\BrowseDropdown;

/* @var $formDTO \common\modules\localization\modules\city\dto\FormDTO */
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$model  = $formDTO->getModel();
$this->params['breadcrumbs'] = [
        [
        'label' => UsniAdaptor::t('application', ($language=='en-US')?'Manage':'Administrar') . ' ' .
        UsniAdaptor::t('city', ($language=='en-US')?'Cities':'Ciudades'),
        'url' => ['/localization/city/default/index']
    ],
        [
        'label' => UsniAdaptor::t('application', ($language=='en-US')?'Update':'Actualizar') . ' #' . $model->id
    ]
];

$browseParams   = ['permission' => 'city.updateother',
                   'data'   => $formDTO->getBrowseModels(),
                   'model'  => $model];
echo BrowseDropdown::widget($browseParams);

$this->title = UsniAdaptor::t('application', ($language=='en-US')?'Update':'Actualizar') . ' ' . UsniAdaptor::t('city', ($language=='en-US')?'City':'Ciudad');
echo $this->render("/_form", ['formDTO' => $formDTO]);
