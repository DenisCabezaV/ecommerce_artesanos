<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */

/* @var $this \usni\library\web\AdminView */
/* @var $formDTO \common\modules\localization\modules\city\dto\FormDTO */

use usni\UsniAdaptor;

$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$this->params['breadcrumbs'] = [
        [
        'label' => UsniAdaptor::t('application', ($language=='en-US')?'Manage':'Administrar') . ' ' .
        UsniAdaptor::t('city', ($language=='en-US')?'Cities':'Ciudades'),
        'url' => ['/localization/city/default/index']
    ],
        [
        'label' => UsniAdaptor::t('application', ($language=='en-US')?'Create':'Crear')
    ]
];
$this->title = UsniAdaptor::t('application', ($language=='en-US')?'Create':'Crear') . ' ' . UsniAdaptor::t('city', ($language=='en-US')?'City':'Ciudad');
echo $this->render("/_form", ['formDTO' => $formDTO]);