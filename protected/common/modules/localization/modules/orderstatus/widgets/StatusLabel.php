<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
namespace common\modules\localization\modules\orderstatus\widgets;

use usni\UsniAdaptor;
use usni\library\bootstrap\Label;
use usni\library\utils\Html;
/**
 * Label for the status
 *
 * @package common\modules\localization\modules\orderstatus\widgets
 */
class StatusLabel extends \yii\bootstrap\Widget
{
    use \common\modules\localization\modules\orderstatus\traits\OrderStatusTrait;
    
    /**
     * @var ActiveRecord|array 
     */
    public $model;
    
    /**
     * @var string 
     */
    public $language;
    
    /**
     * inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->language = UsniAdaptor::app()->languageManager->selectedLanguage;
    }

    /**
     * inheritdoc
     */
    public function run()
    {
        $value      = $this->getOrderStatusLabel($this->model['status'], $this->language);
        $id         = $this->model['id'] . '-status';
        return $this->getLabelWidget($value, $id);
    }
    
    /**
     * Get label widget
     * @param string $value
     * @param string $id
     * @return string
     */
    public function getLabelWidget($value, $id)
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        $labelWidget = null;
        if ($value == UsniAdaptor::t('order', ($language=='en-US')?'Completed':'Completado'))
        {
            $labelWidget = Label::widget(['content' => $value, 'modifier' => Html::COLOR_SUCCESS, 'id' => $id]);
        }
        elseif ($value == UsniAdaptor::t('order', ($language=='en-US')?'Pending':'Pendiente'))
        {
            $labelWidget = Label::widget(['content' => $value, 'modifier' => Html::COLOR_DEFAULT, 'id' => $id]);
        }
        elseif ($value == UsniAdaptor::t('order', ($language=='en-US')?'Cancelled':'Cancelado'))
        {
            $labelWidget = Label::widget(['content' => $value, 'modifier' => Html::COLOR_DANGER, 'id' => $id]);
        }
        elseif ($value == UsniAdaptor::t('order', ($language=='en-US')?'Cancelled Reversal':'Anulación cancelada'))
        {
            $labelWidget = Label::widget(['content' => $value, 'modifier' => Html::COLOR_DEFAULT, 'id' => $id]);
        }
        elseif ($value == UsniAdaptor::t('order', ($language=='en-US')?'Chargeback':'Devolución'))
        {
            $labelWidget = Label::widget(['content' => $value, 'modifier' => Html::COLOR_DEFAULT, 'id' => $id]);
        }
        elseif ($value == UsniAdaptor::t('order', ($language=='en-US')?'Denied':'Denegado'))
        {
            $labelWidget = Label::widget(['content' => $value, 'modifier' => Html::COLOR_DEFAULT, 'id' => $id]);
        }
        elseif ($value == UsniAdaptor::t('order', ($language=='en-US')?'Expired':'Vencido'))
        {
            $labelWidget = Label::widget(['content' => $value, 'modifier' => Html::COLOR_DEFAULT, 'id' => $id]);
        }
        elseif ($value == UsniAdaptor::t('order', ($language=='en-US')?'Failed':'Fallido'))
        {
            $labelWidget = Label::widget(['content' => $value, 'modifier' => Html::COLOR_DEFAULT, 'id' => $id]);
        }
        elseif ($value == UsniAdaptor::t('order', ($language=='en-US')?'Processed':'Procesado'))
        {
            $labelWidget = Label::widget(['content' => $value, 'modifier' => Html::COLOR_DEFAULT, 'id' => $id]);
        }
        elseif ($value == UsniAdaptor::t('order', ($language=='en-US')?'Processing':'Procesando'))
        {
            $labelWidget = Label::widget(['content' => $value, 'modifier' => Html::COLOR_DEFAULT, 'id' => $id]);
        }
        elseif ($value == UsniAdaptor::t('order', ($language=='en-US')?'Refunded':'Devuelto'))
        {
            $labelWidget = Label::widget(['content' => $value, 'modifier' => Html::COLOR_INFO, 'id' => $id]);
        }
        elseif ($value == UsniAdaptor::t('order', ($language=='en-US')?'Reversed':'Anulado'))
        {
            $labelWidget = Label::widget(['content' => $value, 'modifier' => Html::COLOR_WARNING, 'id' => $id]);
        }
        elseif ($value == UsniAdaptor::t('order', ($language=='en-US')?'Shipped':'Enviado'))
        {
            $labelWidget = Label::widget(['content' => $value, 'modifier' => Html::COLOR_DEFAULT, 'id' => $id]);
        }
        elseif ($value == UsniAdaptor::t('order', ($language=='en-US')?'Voided':'Anulado'))
        {
            $labelWidget = Label::widget(['content' => $value, 'modifier' => Html::COLOR_DANGER, 'id' => $id]);
        }
        return $labelWidget;
    }
}