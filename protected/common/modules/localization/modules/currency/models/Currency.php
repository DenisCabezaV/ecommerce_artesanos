<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
namespace common\modules\localization\modules\currency\models;

use usni\library\db\TranslatableActiveRecord;
use usni\UsniAdaptor;
use common\modules\localization\modules\currency\models\CurrencyTranslated;
use yii\db\Exception;
use common\modules\order\dao\OrderDAO;
/**
 * Currency active record.
 * 
 * @package common\modules\localization\modules\currency\models
 */
class Currency extends TranslatableActiveRecord
{
	/**
     * @inheritdoc
     */
	public function rules()
	{
		return [
                    [['name', 'code', 'value'],         'required'],
                    ['name',                            'unique', 'targetClass' => CurrencyTranslated::className(), 'targetAttribute' => ['name', 'language'], 'on' => 'create'],
                    ['name',                            'unique', 'targetClass' => CurrencyTranslated::className(), 'targetAttribute' => ['name', 'language'], 'filter' => ['!=', 'owner_id', $this->id], 'on' => 'update'],
                    ['code',                            'unique', 'on' => 'create'],
                    ['code',                            'unique', 'filter' => ['!=', 'id', $this->id], 'on' => 'update'],
                    ['status',                          'integer'],
                    ['value',                           'number', 'numberPattern' => '/^\s*[-+]?[0-9]*[.,]?[0-9]+([eE][-+]?[0-9]+)?\s*$/'],
                    ['name',                            'string', 'max'=> 64],
                    ['code',                            'string', 'max' => 3],
                    [['symbol_left', 'symbol_right'],   'string', 'max' => 10],
                    ['decimal_place',                   'integer'],
                    [['id', 'name', 'code', 'symbol_left', 'symbol_right', 'decimal_place', 'value', 'status'], 'safe'],
               ];
	}

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenario               = parent::scenarios();
        $scenario['create']     = $scenario['update'] = ['name', 'code', 'status', 'value', 'symbol_left', 'symbol_right', 'decimal_place'];
        $scenario['bulkedit']   = ['status'];
        return $scenario;
    }

    /**
     * @inheritdoc
     */
	public function attributeLabels()
	{
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
		$labels = [
                        'name'					 => UsniAdaptor::t('application', ($language=='en-US')?'Name':'Nombre'),
                        'code'					 => UsniAdaptor::t('localization', ($language=='en-US')?'Code':'Código'),
                        'symbol_left'			 => UsniAdaptor::t('currency', ($language=='en-US')?'Symbol Left':'Icono izquierdo'),
                        'symbol_right'			 => UsniAdaptor::t('currency', ($language=='en-US')?'Symbol Right':'Símbolo Derecho'),
                        'decimal_place'			 => UsniAdaptor::t('currency', ($language=='en-US')?'Decimal Places':'Decimales'),
                        'value'					 => UsniAdaptor::t('application', ($language=='en-US')?'Value':'Valor'),
                        'status'				 => UsniAdaptor::t('application', ($language=='en-US')?'Status':'Estado'),
                  ];
        return parent::getTranslatedAttributeLabels($labels);
	}

    /**
     * @inheritdoc
     */
    public static function getLabel($n = 1)
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return ($n == 1) ? UsniAdaptor::t('currency', ($language=='en-US')?'Currency':'Moneda') : UsniAdaptor::t('currency', ($language=='en-US')?'Currencies':'Monedas');
    }
    
    /**
     * @inheritdoc
     */
    public static function getTranslatableAttributes()
    {
        return ['name'];
    }
    
    /**
     * Check if allowed to delete.
     * 
     * @param Currency $model
     * @return boolean
     */
    public function checkIfAllowedToDelete()
    {
        //Check the orders associated
        $orders = OrderDAO::getOrdersByAttribute('currency_code', $this->code, $this->language);
        if(empty($orders))
        {
            return true;
        }
        return false;
    }
    
    /**
     * inheritdoc
     */
    public function beforeDelete()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        $isAllowedToDelete = $this->checkIfAllowedToDelete();
        if($isAllowedToDelete == false)
        {
            throw new Exception(($language=='en-US')?'This model is associated to orders in the system.':'Este modelo está asociado a las órdenes del sistema.');
        }
        if($this->value == 1.00)
        {
            throw new Exception(($language=='en-US')?'This is base currency for the system and can not be deleted.':'Es la moneda base del sistema y no se puede borrar.');
        }
        return parent::beforeDelete();
    }
}