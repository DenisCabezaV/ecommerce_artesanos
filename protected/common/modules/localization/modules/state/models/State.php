<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
namespace common\modules\localization\modules\state\models;

use usni\library\db\TranslatableActiveRecord;
use usni\UsniAdaptor;
use common\modules\localization\modules\country\models\Country;
use yii\db\Exception;
use common\modules\localization\modules\state\dao\StateDAO;
use taxes\models\Zone;
/**
 * State active record.
 * 
 * @package common\modules\localization\modules\state\models
 */
class State extends TranslatableActiveRecord
{
	/**
     * @inheritdoc
     */
	public function rules()
	{
        return [
                    [['name', 'country_id'],    'required'],
                    ['name',                    'validateName'],
                    ['country_id',              'integer'],
                    ['code',                    'string'],
                    ['name',                    'string', 'max' => 32],
                    [['id', 'name', 'code', 'country_id', 'status'], 'safe'],
               ];
	}
    
    /**
     * Validate Name
     * @param string $attribute
     * @param array  $params
     * @return void
     */
    public function validateName($attribute, $params)
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        $state  = StateDAO::getStateBasedOnNameLanguageAndCountry($this->country_id, $this->name, $this->language);
        if ($state !== false)
        {
            if(($this->scenario == 'create') || ($this->scenario == 'update' && $this->id != $state['id']))
            {
                $countryName = $state['country_name'];
                $this->addError($attribute, UsniAdaptor::t('localization', ($language=='en-US')? "The combination $this->name - $this->language - $countryName of Name, Language and Country has already been taken.": "La combinación $this->name - $this->language - $countryName de Nombre, Idioma y País ya ha sido tomada"));
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenario = parent::scenarios();
        $scenario['create'] = $scenario['update'] = ['name', 'code', 'country_id', 'status'];
        $scenario['bulkedit']   = ['status'];
        return $scenario;
    }

	/**
     * @inheritdoc
     */
	public function attributeLabels()
	{
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
		$labels = [
			'id'            => UsniAdaptor::t('application', 'Id'),
			'country_id'	=> UsniAdaptor::t('country', ($language=='en-US')? 'Country':'País'),
			'name'			=> UsniAdaptor::t('application', ($language=='en-US')? 'Name': 'Nombre'),
			'code'			=> UsniAdaptor::t('localization', ($language=='en-US')?'Code':'Código'),
			'status'		=> UsniAdaptor::t('application', ($language=='en-US')?'Status':'Estado'),
		];
        return parent::getTranslatedAttributeLabels($labels);
	}

	/**
     * @inheritdoc
     */
    public static function getLabel($n = 1)
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return ($n == 1) ? UsniAdaptor::t('state', ($language=='en-US')?'State':'Estado') : UsniAdaptor::t('state', ($language=='en-US')?'States':'Estados');
    }

    /**
     * Get country using a relation.
     * @return ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }
    
    /**
     * @inheritdoc
     */
    public static function getTranslatableAttributes()
    {
        return ['name'];
    }
    
    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        $isAllowedToDelete = $this->checkIfAllowedToDelete();
        if($isAllowedToDelete == false)
        {
            throw new Exception(($language=='en-US')?'This state is associated with Zone.':'Este estado está asociado con la Zona.');
        }
        return parent::beforeDelete();
    }
    
    /**
     * Check if state is allowed to delete.
     * @return boolean
     */
    public function checkIfAllowedToDelete()
    {
        $zoneCount = Zone::find()->where('state_id = :sid', [':sid' => $this->id])->count();
        if($zoneCount > 0)
        {
            return false;
        }
        return true;
    }
}