<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
namespace customer\controllers;

use common\modules\manufacturer\models\Manufacturer;
use frontend\controllers\BaseController;
use usni\library\modules\users\business\Manager;
use usni\library\modules\users\dto\UserFormDTO;
use usni\library\modules\users\models\User;
use usni\UsniAdaptor;
use usni\library\modules\users\utils\UserUtil;
use customer\models\Customer;
use customer\models\LoginForm;
use usni\library\utils\FlashUtil;
use yii\filters\AccessControl;
use usni\library\modules\users\models\Person;
use usni\library\utils\FileUploadUtil;
use common\modules\stores\dao\StoreDAO;
use customer\dto\FormDTO;
use customer\business\SiteManager;
use usni\library\utils\ArrayUtil;
use yii\web\ForbiddenHttpException;
use yii\helpers\Url;
use common\modules\order\dto\GridViewDTO as OrderGridViewDTO;
use usni\library\dto\GridViewDTO;
use yii\web\NotFoundHttpException;
use common\modules\order\dto\DetailViewDTO;
use yii\base\InvalidParamException;
/**
 * SiteController class file.
 * 
 * @package customer\controllers
 */
class SiteController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors          = parent::behaviors();
        $excludedActions    = static::getExcludedActionsFromAccessControl();
        return array_merge($behaviors, [
            'access' => [
                'class' => AccessControl::className(),
                'except' => $excludedActions,
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied by default
                ],
            ],
        ]);
    }
    
    /**
     * Action Logout.
     * @return void
     */
    public function actionLogout()
    {
        UsniAdaptor::app()->user->logout(true);
        UsniAdaptor::app()->cache->flush();
        $this->redirect(UsniAdaptor::createUrl('/customer/site/login'));
    }

    /**
     * Action Login.
     * @return void
     */
    public function actionLogin()
    {
        if(UsniAdaptor::app()->user->isGuest == false)
        {
            return $this->goHome();
        }
        $formDTO    = new FormDTO();
        $formDTO->setModel(new LoginForm());
        $formDTO->setPostData($_POST);
        SiteManager::getInstance()->processLogin($formDTO);
        if($formDTO->getIsTransactionSuccess() == true)
        {
            return $this->goHome();
        }
        else
        {
            return $this->render('/front/login', ['formDTO' => $formDTO]);
        }
    }
    
    /**
     * Action forgot password.
     * @return void
     */
    public function actionForgotPassword()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        if(UsniAdaptor::app()->user->isGuest == false)
        {
            return $this->goHome();
        }
        $formDTO = new FormDTO();
        $postData = UsniAdaptor::app()->request->post();
        $formDTO->setPostData($postData);
        SiteManager::getInstance()->processForgotPassword($formDTO);
        if($formDTO->getActivationStatusIssue() == true)
        {
            FlashUtil::setMessage('warning', UsniAdaptor::t('userflash', ($language=='en-US')?'Your account is not in active status.':'Su cuenta no está en estado activo.'));
        }
        elseif($formDTO->getNotRegisteredEmailId() == true)
        {
            FlashUtil::setMessage('danger', UsniAdaptor::t('userflash', ($language=='en-US')?'The given email is not registered with us. Please enter a valid email.':'El correo electrónico no está registrado con nosotros. Por favor, introduzca un correo electrónico válido.'));
        }
        elseif($formDTO->getIsTransactionSuccess() == true)
        {
            FlashUtil::setMessage('success', UsniAdaptor::t('userflash', ($language=='en-US')?'Your login credentials are sent on registered email address.':'Sus credenciales de inicio de sesión se envían a la dirección de correo electrónico registrada.'));
        }
        return $this->render('/front/forgotpassword', ['formDTO' => $formDTO]);
    }
    
    /**
     * Action register.
     * @return void
     */
    public function actionRegister()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        if(UsniAdaptor::app()->user->isGuest)
        {
            $formDTO = new FormDTO();
            $manager = new SiteManager();
            $customer = UsniAdaptor::app()->request->post('Customer');
            $customer_flag = true;
            if ($customer==null || $customer['groups'] == 1) {
                $formDTO->setScenario('registration');
                $postData = UsniAdaptor::app()->request->post();
                $formDTO->setPostData($postData);
                $manager->processEdit($formDTO);
            } else {
                $customer_flag = false;
                $model = new User(['scenario' => 'create']);
                $manufacturer = new Manufacturer();
                $person = UsniAdaptor::app()->request->post('Person');
                $customer['groups'] = [$customer['groups']];
                $customer['status'] = 1;
                $customer['type'] = 'system';
                $customer['country'] = 'EC';
                $address = UsniAdaptor::app()->request->post('Address');
                $postData = [
                    'User' => $customer,
                    'Person' => $person,
                    'Address' => $address
                ];
                $manufacturer->name = isset($customer['manufacturer']) ? $customer['username'] . '_' .$customer['manufacturer'] : $customer['username'];
                $manufacturer->image = $person['profile_image'];
                $manufacturer->status = 1;
                $manufacturer->save();
                $formDTO = new UserFormDTO();
                $formDTO->setModel($model);
                $formDTO->setPostData($postData);
                $formDTO->setScenario('create');
                $manager = new Manager();
                $manager->processEdit($formDTO);
            }
            if($formDTO->getIsTransactionSuccess() == true)
            {
                if ($customer_flag) {
                    $registration_message = ($language=='en-US') ?
                        'You have successfully registered with the system as a General User. An activation email has been sent at your registered email address.' :
                        'Se ha registrado correctamente en el sistema como un Usuario General. Se ha enviado un correo electrónico de activación a su dirección de correo electrónico registrada.';
                } else {
                    if (isset($customer['manufacturer_description'])) {
                        $description = $customer['manufacturer_description'];
                        $manufacturer_descriptionTable = UsniAdaptor::tablePrefix() . 'manufacturer_description';
                        $manufacturerTable = UsniAdaptor::tablePrefix() . 'manufacturer';
                        $connection = UsniAdaptor::app()->getDb();
                        $last_manufacturer = $connection->createCommand("SELECT id FROM $manufacturerTable ORDER BY id DESC LIMIT 1")->queryScalar();
                        $connection->createCommand()->insert($manufacturer_descriptionTable, [
                            'owner_id' => $last_manufacturer,
                            'description' => $description
                        ])->execute();
                    }
                    $artesanos = ($language == 'en-US') ? 'Artesanal System' : 'Sistema Artesanal';
                    $registration_message = ($language=='en-US') ?
                        'You have successfully registered with the Artesanal System. To access system please follow this link <a href="' . UsniAdaptor::createUrl('backend/') . '" id="artesanal-system" title="' . $artesanos . '" class="top-nav-wishlist">
                            <span class="hidden-xs hidden-sm hidden-md">' . $artesanos . '</span>
                        </a>':
                        'Se ha registrado correctamente en el Sistema Artesanal. Para acceder al sistema por favor siga el siguiente enlace <a href="' . UsniAdaptor::createUrl('backend/') . '" id="artesanal-system" title="' . $artesanos . '" class="top-nav-wishlist">
                            <span class="hidden-xs hidden-sm hidden-md">' . $artesanos . '</span>
                        </a>';
                }

                FlashUtil::setMessage('success', UsniAdaptor::t('userflash', $registration_message));
                return $this->redirect(UsniAdaptor::createUrl('customer/site/login'));
            }
            else
            {
                return $this->render('/front/registration', ['formDTO' => $formDTO]);
            }
        }
        else
        {
            $customer = UsniAdaptor::app()->user->getIdentity();
            $this->redirect(UsniAdaptor::createUrl('/customer/site/edit-profile', ['id' => $customer->id]));
        } 
    }
    
    /**
     * Action EditProfile.
     * @return void
     */
    public function actionEditProfile($id = null)
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        if(UsniAdaptor::app()->user->isGuest == false)
        {
            if($id == null)
            {
                $id = UsniAdaptor::app()->user->getIdentity()->id;
            }
            $formDTO = new FormDTO();
            $formDTO->setScenario('editprofile');
            $postData = UsniAdaptor::app()->request->post();
            $formDTO->setPostData($postData);
            $formDTO->setId($id);
            $manager = new SiteManager();
            $manager->processEdit($formDTO);
            if($formDTO->getIsTransactionSuccess())
            {
                FlashUtil::setMessage('success', UsniAdaptor::t('applicationflash', ($language=='en-US')?'Record has been updated successfully.':'El registro se ha actualizado con éxito.'));
            }
            return $this->render('/front/editprofile', ['formDTO' => $formDTO]);
        }
        $this->redirect(UsniAdaptor::createUrl('/site/default/index'));
    }

    /**
     * My account dashboard.
     * @return void
     */
    public function actionMyAccount()
    {
        return $this->render('/front/myaccount', ['model' => UsniAdaptor::app()->user->getIdentity()]);
    }
    
    /**
     * Change password for user.
     * @return void
     */
    public function actionChangePassword()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        $customer   = UsniAdaptor::app()->user->getIdentity();
        $postData   = ArrayUtil::getValue($_POST, ['ChangePasswordForm']);
        $formDTO    = new FormDTO();
        $formDTO->setPostData($postData);
        $formDTO->setId($customer->id);
        $formDTO->setModelClassName(Customer::className());
        $manager    = new SiteManager();
        $manager->processChangePassword($formDTO);
        UsniAdaptor::app()->getSession()->setFlash('warning', UserUtil::getPasswordInstructions());
        if($formDTO->getIsTransactionSuccess() === true)
        {
            FlashUtil::setMessage('success', UsniAdaptor::t('userflash', ($language=='en-US')?'Password has been changed successfully.':'La contraseña ha sido cambiada con éxito.'));
            return $this->refresh();
        }
        elseif($formDTO->getIsTransactionSuccess() === false)
        {
            throw new ForbiddenHttpException(\Yii::t('yii',($language=='en-US')?'You are not authorized to perform this action.':'Usted no está autorizado a realizar esta acción.'));
        }
        else
        {
            return $this->render('/front/changepassword', ['formDTO' => $formDTO]);
        }
    }

    /**
     * Validate email address.
     * @param $hash string
     * @param $email string
     * @return void
     */
    public function actionValidateEmailAddress($hash, $email)
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        if (UsniAdaptor::app()->user->getIsGuest())
        {
            $result = SiteManager::getInstance()->processValidateEmailAddress($hash, $email);
            if($result == true)
            {
                FlashUtil::setMessage('success', UsniAdaptor::t('users', ($language=='en-US')?'Your email has been validated, Please login to continue.':'Su correo electrónico ha sido validado. Por favor, inicie sesión para continuar.'));
            }
            elseif($result == false)
            {
                FlashUtil::setMessage('error', UsniAdaptor::t('users', ($language=='en-US')?'Your email validation fails. Please contact system admin.':'Su validación de correo electrónico falla. Póngase en contacto con el administrador del sistema.'));
            }
            return $this->redirect(UsniAdaptor::createUrl('customer/site/login'));
        }
        return $this->redirect(Url::home());
    }

    /**
     * Change user currency in the configuration.
     * @param string $currency
     * @return void
     */
    public function actionSetCurrency($currency)
    {
        if($currency != null)
        {
            UsniAdaptor::app()->cookieManager->setCurrencyCookie($currency);
        }
    }
    
    /**
     * Change store in the configuration.
     * @param string $id
     * @return void
     */
    public function actionSetStore($id)
    {
        if($id != null)
        {
            $store          = StoreDAO::getById($id, UsniAdaptor::app()->languageManager->selectedLanguage);
            if(!empty($store))
            {
                UsniAdaptor::app()->cookieManager->setStoreCookie($id);
                $currency = UsniAdaptor::app()->storeManager->getLocalValue('currency', $id);
                UsniAdaptor::app()->cookieManager->setCurrencyCookie($currency);
                $language = UsniAdaptor::app()->storeManager->getLocalValue('language', $id);
                UsniAdaptor::app()->cookieManager->setLanguageCookie($language);
            }
            return $this->redirect(UsniAdaptor::app()->request->referrer);
        }
    }
    
    /**
     * Render order history
     * @return string
     */
    public function actionOrderHistory()
    {
        $gridViewDTO = new OrderGridViewDTO();
        SiteManager::getInstance()->processOrderHistory($gridViewDTO, $_GET);
        return $this->render('/front/order/myorders', ['gridViewDTO' => $gridViewDTO]);
    }
    
    /**
     * Render order view.
     * @param $id OrderId
     * @return string
     */
    public function actionOrderView($id)
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        $detailViewDTO = new DetailViewDTO();
        $detailViewDTO->setId($id);
        SiteManager::getInstance()->processOrderView($detailViewDTO, UsniAdaptor::app()->storeManager->selectedStoreId);
        if($detailViewDTO->getEmptyOrder() == true)
        {
            throw new InvalidParamException(UsniAdaptor::t('order', ($language=='en-US')?"Invalid order":"Pedido no válido"));
        }
        elseif($detailViewDTO->getIsValidOrder() == true)
        {
            return $this->render('/front/order/view', ['detailViewDTO' => $detailViewDTO]);
        }
        else
        {
            throw new InvalidParamException(UsniAdaptor::t('order', ($language=='en-US')?"Invalid order":"Pedido no válido"));
        }
    }
    
    /**
     * Change language for the application.
     * @param string $language
     * @return void.
     */
    public function actionChangeLanguage($language = null)
    {
        if($language != null)
        {
            UsniAdaptor::app()->cookieManager->setLanguageCookie($language);
        }
    }
    
    /**
     * Gets excluded actions from access control.
     * @return array
     */
    protected static function getExcludedActionsFromAccessControl()
    {
        return ['login', 'forgot-password', 'register', 'set-store', 'set-currency', 'change-language', 'validate-email-address'];
    }
    
    /**
     * Delete image.
     * @param integer $id.
     * @return void.
     */
    public function actionDeleteImage()
    {
        if(UsniAdaptor::app()->request->isAjax)
        {
            $id         = $_GET['id'];
            $model              = Person::findOne($id);
            $imageFieldName     = 'profile_image';
            $model->scenario    = 'deleteimage';
            FileUploadUtil::deleteImage($model, $imageFieldName, 150, 150);
            $model->$imageFieldName = null;
            $model->save();
        }
    }
    
    /**
     * Render my downloads
     * @return string
     */
    public function actionDownloads()
    {
        $gridViewDTO = new GridViewDTO();
        SiteManager::getInstance()->processDownloads($gridViewDTO, $_GET);
        return $this->render('/front/mydownloads', ['gridViewDTO' => $gridViewDTO]);
    }
    
    /**
     * Download file.
     * @param int $id
     * @param int $orderId
     */
    public function actionDownload($id, $orderId)
    {
        $result = SiteManager::getInstance()->processDownload($id, $orderId, UsniAdaptor::app()->storeManager->selectedStoreId);
        if($result == false)
        {
            throw new NotFoundHttpException();
        }
        if($result !== false && !empty($result))
        {
            FlashUtil::setMessage('error', $result);
            return $this->redirect('customer/site/downloads');
        }
    }
}