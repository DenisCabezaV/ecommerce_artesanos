<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
use usni\UsniAdaptor;
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$this->params['breadcrumbs'] = [    
                                    [
                                        'label' => UsniAdaptor::t('application', ($language=='en-US')?'Manage':'Administrar') . ' ' . UsniAdaptor::t('customer', ($language=='en-US')?'Customer Groups':'Grupo de clientes'),
                                        'url'   => ['/customer/group/index']
                                    ],
                                    [
                                        'label' => UsniAdaptor::t('application', ($language=='en-US')?'Create':'Crear')
                                    ]
                               ];

$this->title = UsniAdaptor::t('application', ($language=='en-US')?'Create':'Crear') . ' ' . UsniAdaptor::t('customer', ($language=='en-US')?'Customer Groups':'Grupo de clientes');
echo $this->render('/groups/_form', ['formDTO' => $formDTO]);