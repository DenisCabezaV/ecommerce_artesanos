<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
use usni\UsniAdaptor;
use usni\library\modules\auth\widgets\BrowseDropdown;

/* @var $formDTO \usni\library\modules\auth\dto\FormDTO */
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$model  = $formDTO->getModel();
$this->params['breadcrumbs'] = [
        [
        'label' => UsniAdaptor::t('application', ($language=='en-US')?'Manage':'Administrar') . ' ' .
        UsniAdaptor::t('customer', ($language=='en-US')?'Customer Groups':'Grupo de clientes'),
        'url' => ['/customer/group/index']
    ],
        [
        'label' => UsniAdaptor::t('application', ($language=='en-US')?'Update':'Actualizar') . ' #' . $model->id
    ]
];

$browseParams   = ['permission' => 'group.updateother',
                   'data'   => $formDTO->getBrowseModels(),
                   'model'  => $model];
echo BrowseDropdown::widget($browseParams);

$this->title = UsniAdaptor::t('application', ($language=='en-US')?'Update':'Actualizar') . ' ' . UsniAdaptor::t('auth', ($language=='en-US')?'Group':'Grupo');
echo $this->render("/groups/_form", ['formDTO' => $formDTO]);