<?php 
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
use usni\library\utils\Html;
use usni\UsniAdaptor;
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
?>

<!-- Sidebar navigation -->
<div class="col-sm-3 hidden-xs" id="column-left">
<nav>
  <ul id="accountsidebarlist">
      <li><?php echo Html::a(UsniAdaptor::t('users', ($language=='en-US')?'My Account':'Mi cuenta'), UsniAdaptor::createUrl('/customer/site/my-account')); ?></li>
      <li><?php echo Html::a(UsniAdaptor::t('users', ($language=='en-US')?'Edit Profile':'Editar perfil'), UsniAdaptor::createUrl('/customer/site/edit-profile')); ?></li>
      <li><?php echo Html::a(UsniAdaptor::t('users', ($language=='en-US')?'Change Password':'Cambiar Contraseña'), UsniAdaptor::createUrl('/customer/site/change-password')) ?></li>
      <li><?php echo Html::a(UsniAdaptor::t('order', ($language=='en-US')?'My Orders':'Mis Pedidos'), UsniAdaptor::createUrl('/customer/site/order-history')) ?></li>
      <li><?php echo Html::a(UsniAdaptor::t('wishlist', ($language=='en-US')?'Wishlist':'Lista de deseos'), UsniAdaptor::createUrl('wishlist/default/view')) ?></li>
      <li><?php echo Html::a(UsniAdaptor::t('customer', ($language=='en-US')?'My Downloads':'Mis Descargas'), UsniAdaptor::createUrl('customer/site/downloads')) ?></li>
  </ul>
</nav>
</div>

