<?php
use yii\helpers\Html;
$model = $formDTO->getAddress(); 
?>
<?= $form->field($model, 'address1')->textInput();?>
<?= $form->field($model, 'address2')->textInput();?>
<?= $form->field($model, 'city')->textInput();?>
<?= $form->field($model, 'state')->textInput();?>
<?= Html::activeHiddenInput($model, 'country', ['value' => 'EC']);?>
<?= Html::activeHiddenInput($model, 'postal_code', ['value' => '080101']);?>