<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
use usni\UsniAdaptor;
use frontend\widgets\ActiveForm;
use frontend\widgets\FormButtons;
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$title              = UsniAdaptor::t('users', ($language=='en-US')?'Forgot Password':'Olvidé la contraseña');
$this->title        = $title;
$model              = $formDTO->getModel();
$descriptionText    = '<p style="margin:10px">' . UsniAdaptor::t('customer', ($language=='en-US')?'Enter the e-mail address associated with your account. Click submit to have your information e-mailed to you.':'Introduzca la dirección de correo electrónico asociada a su cuenta. Haga clic en Enviar para que le envíen su información por correo electrónico.') . "</p>";
$this->params['breadcrumbs'] = [    
                                    [
                                        'label' => UsniAdaptor::t('customer', ($language=='en-US')?'My Account':'Mi cuenta'),
                                        'url'   => ['/customer/site/my-account']
                                    ],
                                    [
                                        'label' => $title
                                    ]
                               ];
$form = ActiveForm::begin([
                            'id'          => 'forgotpasswordformview',
                            'layout'      => 'horizontal',
                            'caption'     => UsniAdaptor::t('users', ($language=='en-US')?'Forgot Password':'Olvidé la contraseña') . '?'
                       ]);
?>
<?= $descriptionText;?>
<?= $form->field($model, 'email')->textInput();?>
<?= FormButtons::widget(['cancelUrl' => UsniAdaptor::createUrl('customer/site/login'), 'submitButtonLabel' => UsniAdaptor::t('application', ($language=='en-US')?'Submit':'Enviar')]);?>
<?php ActiveForm::end();?>
