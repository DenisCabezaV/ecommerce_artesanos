<?php
use usni\UsniAdaptor;
use usni\library\utils\DateTimeUtil;
use common\modules\localization\modules\orderstatus\widgets\StatusLabel;

/* @var $detailViewDTO \common\modules\order\dto\DetailViewDTO */
/* @var $this \usni\library\web\AdminView */
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$rows = $detailViewDTO->getHistoryRecords();
?>
<div id="order-history">
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th><?php echo UsniAdaptor::t('customer', ($language=='en-US')?'Status':'Estado'); ?></th>
                    <th><?php echo UsniAdaptor::t('customer', ($language=='en-US')?'Date Added':'Fecha en que se agregó'); ?></th>
                    <th><?php echo UsniAdaptor::t('application', ($language=='en-US')?'Comment':'Comentario'); ?></th>
                    <th><?php echo UsniAdaptor::t('customer', ($language=='en-US')?'Customer Notified':'Aviso al cliente'); ?></th>
                </tr>
            </thead>
            <?php
            if (!empty($rows))
            {
                ?>
                <tbody>
                    <?php
                    foreach ($rows as $row)
                    {
                        ?>
                        <tr>
                            <td><?php echo StatusLabel::widget(['model' => $row]);?></td>
                            <td><?php echo DateTimeUtil::getFormattedDateTime($row['created_datetime']) ?></td>
                            <td><?php echo $row['comment'] ?></td>
                            <td><?php echo (bool) $row['notify_customer'] == true ? UsniAdaptor::t('application', ($language=='en-US')?'Yes':'Si') : UsniAdaptor::t('application', ($language=='en-US')?'No':'No') ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
                <?php
            }
            else
            {
                ?>
                <tbody>
                    <tr><td colspan="6"><?php echo \Yii::t('yii', ($language=='en-US')?'No results found.':'No se han encontrado resultados.'); ?></td></tr>
                </tbody>
                <?php
            }
            ?>
        </table>
    </div>
</div>
<br/>