<?php

use customer\models\Customer;
use yii\helpers\Html;
use usni\UsniAdaptor;
use yii\db\Query;

$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$group_label = ($language == 'es-EC' ? 'Tipo de Usuario' : 'User Type');
$manufacturer_label = ($language == 'es-EC' ? 'Fabricante' : 'Manufacturer');
$manufacturer_description_label = ($language == 'es-EC' ? 'Descripción del Fabricante<br><strong>Ingrese aquí todo lo relacionado al contacto que tendrá el cliente con usted como artesano<br>Todos los datos descritos aquí serán enviados al cliente al momento que este realice una compra</strong>' :
    'Manufacturer Description<br><strong>Write here all the information related to you as an Artesan<br>All the information described here will be sended to the customer when buying something</strong>');

$model = $formDTO->getModel();

$table = UsniAdaptor::tablePrefix() . 'group';
$query = new Query();
$query->select(['name', 'id'])->from(["$table g"]);
$results = $query->all();
$groups = array();
foreach ($results as $key => $value) {
    $groups[$value['id']] = $value['name'];
}
if ($model->scenario != 'editprofile') { ?>
    <?= $form->field($model, 'username')->textInput(); ?>
<?php }

if ($model->scenario == 'registration') { ?>
    <?= Html::activeHiddenInput($model, 'status', ['value' => Customer::STATUS_PENDING]); ?>
<?php } else { ?>
    <?= Html::activeHiddenInput($model, 'status', ['value' => $model->status]); ?>
<?php } ?>
<?= Html::activeHiddenInput($model, 'timezone', ['value' => 'America/Guayaquil']); ?>
<?php if ($model->scenario == 'create' || $model->scenario == 'registration') { ?>
    <?= $form->field($model, 'groups')
        ->select2input($groups, true, [
            'onchange' => 'select2input_onchange()'
        ])
        ->label($group_label)?>
    <div id="manufacturer-div" style="display: none">
        <div class="form-group">
            <label class="control-label col-sm-2" for="manufacturer-input">
                <?= $manufacturer_label ?>
            </label>
            <div class="col-sm-10">
                <input id="manufacturer-input" class="form-control" type="text" name="Customer[manufacturer]"
                       data-toggle="tooltip" data-trigger="hover" data-placement="top" value="" aria-required="true" aria-invalid="true">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="manufacturer-description-input">
                <?= $manufacturer_description_label ?>
            </label>
            <div class="col-sm-10">
                <?= \marqu3s\summernote\Summernote::widget([
                    'id' => 'manufacturer-description-input',
                    'name' => 'Customer[manufacturer_description]'
                ]); ?>
            </div>
        </div>
    </div>
    <?= $form->field($model, 'password')->passwordInput() ?>
    <?= $form->field($model, 'confirmPassword')->passwordInput() ?>
<?php }

$select2input_onchange = <<<JS
function select2input_onchange() {
    var manufacturer = $('#manufacturer-div');
    var group = $('#customer-groups').val();
    if (group == 2) {
        manufacturer.css('display', 'block');
    } else {
        manufacturer.css('display','none');
    }
}
JS;

$this->registerJs($select2input_onchange, \frontend\web\View::POS_HEAD);

?>
