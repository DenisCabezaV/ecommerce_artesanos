<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */

/* @var $form \usni\library\bootstrap\TabbedActiveForm */
/* @var $this \usni\library\web\AdminView */
/* @var $userFormDTO \usni\library\modules\users\dto\UserFormDTO */

use usni\UsniAdaptor;
use usni\library\modules\users\widgets\BrowseDropdown;
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$customer = $formDTO->getModel();
$this->params['breadcrumbs'] = [
                                    [
                                        'label' => UsniAdaptor::t('application', ($language=='en-US')?'Manage':'Administrar') . ' ' . UsniAdaptor::t('customer', ($language=='en-US')?'Customers':'Clientes'),
                                        'url'   => ['/customer/default/index']
                                    ],
                                    [
                                        'label' => UsniAdaptor::t('application', ($language=='en-US')?'Update':'Actualizar') . ' #' . $customer['id']
                                    ]
                               ];
$browseParams   = ['permission' => 'customer.updateother',
                   'data' => $formDTO->getBrowseModels(),
                   'model' => $customer,
                   'textAttribute' => 'username'];
echo BrowseDropdown::widget($browseParams);
$this->title = UsniAdaptor::t('application', ($language=='en-US')?'Update':'Actualizar') . ' ' . UsniAdaptor::t('customer', ($language=='en-US')?'Customer':'Cliente');
echo $this->render('/_tabform', ['formDTO' => $formDTO]);