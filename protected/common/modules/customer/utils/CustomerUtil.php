<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
namespace customer\utils;

use usni\UsniAdaptor;
use usni\library\utils\ArrayUtil;
use usni\library\modules\users\models\Address;
/**
 * Contains utility functions related to Customer.
 *
 * @package customer\utils
 */
class CustomerUtil
{
    /**
     * Get address type dropdown.
     * @return array
     */
    public static function getAddressTypeDropdown()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return [
                    Address::TYPE_SHIPPING_ADDRESS  => UsniAdaptor::t('customer', ($language=='en-US')?'Shipping Address':'Dirección de envío'),
                    Address::TYPE_BILLING_ADDRESS   => UsniAdaptor::t('customer', ($language=='en-US')?'Billing Address':'Dirección de facturación'),
                    Address::TYPE_DEFAULT           => UsniAdaptor::t('customer', ($language=='en-US')?'Default Address':'Dirección por defecto')
               ];
    }
    
    /**
     * Get email for the user.
     * @param string $username
     * @param string $email
     * @return string
     */
    public static function getEmail($username, $email)
    {
        if ($email == null)
        {
            return $username . '@whatacart.com';
        }
        else
        {
            return $email;
        }
    }
    
    /**
     * Get formatted customer activity key.
     * @param string $key
     * @return mixed
     */
    public static function getFormattedCustomerActivityKey($key)
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        $formattedKeys = [
                            'new_registration'      => UsniAdaptor::t('customer', ($language=='en-US')?'registered for an account.':'registrado para una cuenta.'),
                            'account_order_created' => UsniAdaptor::t('customer', ($language=='en-US')?'created a new order.':'ha creado una nueva orden.'),
                            'login'                 => UsniAdaptor::t('customer', ($language=='en-US')?'logged in.':'ha iniciado sesión.'),
                            'edit_profile'          => UsniAdaptor::t('customer', ($language=='en-US')?'updated their account details.':'actualizaron los detalles de su cuenta.'),
                            'forgot_password'       => UsniAdaptor::t('customer', ($language=='en-US')?'requested a new password.':'solicitó una nueva contraseña.'),
                            'change_password'       => UsniAdaptor::t('customer', ($language=='en-US')?'updated their account password.':'actualizado la contraseña de su cuenta.'),
                            'add_address'           => UsniAdaptor::t('customer', ($language=='en-US')?'added a new address.':'agregó una nueva dirección.')
                         ];
        return ArrayUtil::getValue($formattedKeys, $key);
    }
}