<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl.html
 */
namespace common\modules\payment\models\cashondelivery;

use usni\UsniAdaptor;
/**
 * CashOnDeliverySetting class file.
 *
 * @package common\modules\payment\models\cashondelivery
 */
class CashOnDeliverySetting extends \yii\base\Model
{
    /**
     * @var string 
     */
    public $order_status;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
                    ['order_status', 'required'],
               ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return [
                    'order_status'  => UsniAdaptor::t('orderstatus', ($language=='en-US')?'Order Status':'Estado del pedido')
               ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return [
                    'order_status'  => UsniAdaptor::t('payment', ($language=='en-US')?'Default order status':'Estado del pedido por defecto')
               ];
    }
}
