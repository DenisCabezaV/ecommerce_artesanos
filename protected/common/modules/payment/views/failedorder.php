<?php
use usni\UsniAdaptor;
use yii\helpers\Url;

$userModel = UsniAdaptor::app()->user->getIdentity();
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
/* @var $this \frontend\web\View */
$this->params['breadcrumbs'] = [
        [
        'label' => UsniAdaptor::t('cart', ($language=='en-US')?'Shopping Cart':'Carrito de compras'),
        'url' => UsniAdaptor::createUrl('cart/default/view')
    ],
        [
        'label' => UsniAdaptor::t('cart', ($language=='en-US')?'Checkout':'Realizar Pedido'),
        'url' => UsniAdaptor::createUrl('cart/checkout/index')
    ],
        [
        'label' => UsniAdaptor::t('cart', ($language=='en-US')?'Incomplete Order':'Pedido incompleto')
    ]
];

$this->title = UsniAdaptor::t('order', ($language=='en-US')?'Incomplete Order':'Pedido incompleto');
?>
<div class="well">
    <p><?php echo UsniAdaptor::t('cart', ($language=='en-US')?"The order is incomplete.":"El pedido está incompleto."); ?>
        <?php
        if ($userModel != null)
        {
            $myOrdersUrl = UsniAdaptor::createUrl('customer/site/order-history');
            echo "Please view your order under <a href='" . $myOrdersUrl . "'>" . UsniAdaptor::t('order', ($language=='en-US')?'My Orders':'Mis Pedidos') . "</a> section.";
        }
        ?>
    </p>
    <p>Please contact <a href="mailto:support@whatacart.com?Subject=Query" target="_top">system admin.</a></p>
    <div class="buttons text-right">
        <a href="<?php echo Url::home() ?>" class="btn btn-success"><?php echo UsniAdaptor::t('application', ($language=='en-US')?'Continue':'Continuar'); ?></a>
    </div>
</div>
