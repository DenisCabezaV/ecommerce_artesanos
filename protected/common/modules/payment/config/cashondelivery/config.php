<?php
use usni\UsniAdaptor;
use common\modules\extension\models\Extension;
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
return [
        'code'      => 'cashondelivery',
        'name'      => UsniAdaptor::t('payment', ($language=='en-US')?'Cash On Delivery':"Efectivo"),
        'author'    => 'WhatACart',
        'version'   => '1.0',
        'product_version' => '2.0.0',
        'status'    => Extension::STATUS_INACTIVE,
        'category'  => 'payment'
        ];