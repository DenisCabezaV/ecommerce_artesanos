<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
namespace common\modules\extension\models;

use usni\library\db\TranslatableActiveRecord;
use usni\library\db\ActiveRecord;
use usni\UsniAdaptor;
/**
 * Extension class file. This acts as model class for the extension table.
 *
 * @package common\modules\extension\models
 */
class Extension extends TranslatableActiveRecord
{
	/**
     * @inheritdoc
     */
	public function rules()
	{
		return [
                    [['name', 'category', 'code'],       'required'],
                    [['status'],               'integer'],
                    ['status',                 'default', 'value' => ActiveRecord::STATUS_ACTIVE],
                    [['name', 'author', 'version', 'product_version', 'status', 'data'],       'safe'],
               ];
	}
    
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenario           = parent::scenarios();
        $scenario['create'] = $scenario['update'] = ['name', 'author', 'version', 'product_version', 'status', 'data', 'category', 'code'];
        return $scenario;
    }
    
    /**
     * @inheritdoc
     */
	public function attributeLabels()
	{
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
		return [
                    'name'          => UsniAdaptor::t('application', ($language=='en-US')?'Name':'Nombre'),
                    'author'        => UsniAdaptor::t('application', ($language=='en-US')?'Author':'Autor'),
                    'version'       => UsniAdaptor::t('application', ($language=='en-US')?'Version':'Versión'),
                    'product_version'     => UsniAdaptor::t('application', ($language=='en-US')?'Product Version':'Versión del producto'),
                    'status'        => UsniAdaptor::t('application', ($language=='en-US')?'Status':'Estado'),
                    'data'          => UsniAdaptor::t('extension', ($language=='en-US')?'Configuration Data':'Datos de configuración'),
                    'category'      => UsniAdaptor::t('application', ($language=='en-US')?'Category':'Categoría'),
                    'code'          => UsniAdaptor::t('extension', ($language=='en-US')?'Code':'Código')
               ];
	}
    
    /**
     * @inheritdoc
     */
    public static function getLabel($n = 1)
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return ($n == 1) ? UsniAdaptor::t('extension', ($language=='en-US')?'Extension':'Extensión') : UsniAdaptor::t('extension', ($language=='en-US')?'Extensions':'Extensiones');
    }
    
    /**
     * @inheritdoc
     */
    public static function getTranslatableAttributes()
    {
        return ['name'];
    }
    
    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if(parent::beforeSave($insert))
        {
            if(is_array($this->product_version))
            {
                $this->product_version = implode(',', $this->product_version);
            }
            return true;
        }
        else
        {
            return false;
        }
    }
}