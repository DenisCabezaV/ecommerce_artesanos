<?php
use usni\library\utils\MenuUtil;
use usni\UsniAdaptor;
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
if(UsniAdaptor::app()->user->can('access.stores'))
{
    return [    
                'label'       => MenuUtil::getSidebarMenuIcon('globe') .
                                     MenuUtil::wrapLabel(UsniAdaptor::t('stores', ($language=='en-US')?'Stores':'Tiendas')),
                'url'         => ['/stores/default/index'],
                'itemOptions' => ['class' => 'navblock-header']
            ];
}
return [];

