<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
namespace common\modules\stores\models;

use usni\UsniAdaptor;
use yii\validators\FileValidator;
/**
 * StoreImage active record.
 * 
 * @package common\modules\stores\models
 */
class StoreImage extends \yii\base\Model
{
    /**
     * Upload logo Instance.
     * @var string
     */
    public $logoUploadInstance;
    /**
     * logo saved image.
     * @var string
     */
    public $logoSavedImage;
    /**
     * Upload icon Instance.
     * @var string
     */
    public $iconUploadInstance;
    /**
     * Upload icon Instance.
     * @var string
     */
    public $iconSavedImage;
    
    //Form variables
    public $store_logo;
    public $icon;
    public $category_image_width;
    public $category_image_height;
    public $product_list_image_width; 
    public $product_list_image_height;
    public $related_product_image_width; 
    public $related_product_image_height;
    public $compare_image_width; 
    public $compare_image_height;
    public $wishlist_image_width;
    public $wishlist_image_height; 
    public $cart_image_width;
    public $cart_image_height;
    public $store_image_width;
    public $store_image_height;
	/**
     * @inheritdoc
     */
	public function rules()
	{
		return [
                    [['category_image_width', 'category_image_height', 'product_list_image_width', 'product_list_image_height',
                      'related_product_image_width', 
                      'related_product_image_height', 'compare_image_width', 'compare_image_height', 'wishlist_image_width', 'wishlist_image_height', 
                      'cart_image_width', 'cart_image_height', 'store_image_width', 'store_image_height'],   'required'],
                    [['category_image_width', 'category_image_height', 'product_list_image_width', 'product_list_image_height',
                      'related_product_image_width', 
                      'related_product_image_height', 'compare_image_width', 'compare_image_height', 'wishlist_image_width', 'wishlist_image_height', 
                      'cart_image_width', 'cart_image_height', 'store_image_width', 'store_image_height'],   'number', 'integerOnly' => true],
                    [['icon', 'iconUploadInstance'], 'image', 'skipOnEmpty' => true, 'extensions' => 'ico', 'checkExtensionByMimeType' => false],
                    [['icon', 'iconUploadInstance'], FileValidator::className(), 'minSize' => 1150, 'maxSize' => 155648],
                    [['store_logo', 'logoUploadInstance'], 'image', 'skipOnEmpty' => true, 'extensions' => 'jpg, png, gif, jpeg'],
                    [['store_logo', 'icon', 'category_image_width', 'category_image_height', 'product_list_image_width', 
                      'product_list_image_height', 'related_product_image_width', 
                      'related_product_image_height', 'compare_image_width', 'compare_image_height', 'wishlist_image_width', 'wishlist_image_height', 
                      'cart_image_width', 'cart_image_height', 'store_image_width', 'store_image_height'],   'safe'],
               ];
	}

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenario = parent::scenarios();
        $scenario['create'] = $scenario['update'] = ['store_logo', 'icon', 'category_image_width', 'category_image_height', 
        'product_list_image_width', 'product_list_image_height', 
        'related_product_image_width', 'related_product_image_height', 'compare_image_width', 'compare_image_height', 'wishlist_image_width', 
        'wishlist_image_height', 'cart_image_width', 'cart_image_height', 'store_image_width', 'store_image_height'];
        return $scenario;
    }
    
    /**
     * @inheritdoc
     */
	public function attributeLabels()
	{
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
		return [
                        'store_logo'                        => UsniAdaptor::t('stores',($language=='en-US')?'Store Logo':'Logo de la tienda'),
                        'icon'                              => UsniAdaptor::t('stores', ($language=='en-US')?'Icon':'Icono'),
                        'category_image_width'              => UsniAdaptor::t('stores', ($language=='en-US')?'Category Image Width':'Categoría Ancho de imagen'),
                        'category_image_height'             => UsniAdaptor::t('stores', ($language=='en-US')?'Category Image Height':'Categoría Altura de la imagen'),
                        'product_list_image_width'          => UsniAdaptor::t('stores', ($language=='en-US')?'Product List Image Width':'Ancho de imagen de la lista de productos'),
                        'product_list_image_height'         => UsniAdaptor::t('stores', ($language=='en-US')?'Product List Image Height':'Lista de productos Altura de la imagen'),
                        'related_product_image_width'       => UsniAdaptor::t('stores', ($language=='en-US')?'Related Product Image Width':'Ancho de imagen de producto relacionado'),
                        'related_product_image_height'      => UsniAdaptor::t('stores', ($language=='en-US')?'Related Product Image Height':'Altura de la imagen del producto relacionado'),
                        'compare_image_width'               => UsniAdaptor::t('stores', ($language=='en-US')?'Compare Image Width':'Comparar el ancho de imagen'),
                        'compare_image_height'              => UsniAdaptor::t('stores', ($language=='en-US')?'Compare Image Height':'Comparar la altura de la imagen'),
                        'wishlist_image_width'              => UsniAdaptor::t('stores', ($language=='en-US')?'Wishlist Image Width':'Ancho de la imagen de la lista de deseos'),
                        'wishlist_image_height'             => UsniAdaptor::t('stores', ($language=='en-US')?'Wishlist Image Height':'Altura de la imagen de la lista de deseos'),
                        'cart_image_width'                  => UsniAdaptor::t('stores', ($language=='en-US')?'Cart Image Width':'Ancho de la imagen del carro'),
                        'cart_image_height'                 => UsniAdaptor::t('stores', ($language=='en-US')?'Cart Image Height':'Altura de la imagen del carro'),
                        'store_image_width'                 => UsniAdaptor::t('stores', ($language=='en-US')?'Store Image Width':'Ancho de imagen de la tienda'),
                        'store_image_height'                => UsniAdaptor::t('stores', ($language=='en-US')?'Store Image Height':'Altura de la imagen de la tienda')
                  ];
	}
    
    /**
     * @inheritdoc
     */
    public static function getLabel($n = 1)
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return UsniAdaptor::t('application', ($language=='en-US')?'Image':'Imagen');
    }
}