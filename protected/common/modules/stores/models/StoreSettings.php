<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
namespace common\modules\stores\models;

use usni\UsniAdaptor;
/**
 * StoreSettings active record.
 *
 * @package common\modules\stores\models
 */
class StoreSettings extends \yii\base\Model
{   
    public $invoice_prefix;
    public $catalog_items_per_page;
    public $list_description_limit;
    public $display_price_with_tax;
    public $tax_calculation_based_on;
    public $guest_checkout; 
    public $order_status;
    public $display_stock;
    public $customer_online;
    public $default_customer_group;
    public $allow_reviews;
    public $allow_guest_reviews;
    public $show_out_of_stock_warning;
    public $allow_out_of_stock_checkout;
    public $allow_wishlist;
    public $allow_compare_products; 
    public $customer_prefix;
    public $order_prefix;
    public $display_weight;
    public $display_dimensions;


    /**
     * @inheritdoc
     */
	public function rules()
	{
		return [
                    [['invoice_prefix', 'catalog_items_per_page', 'list_description_limit', 'customer_prefix', 'order_prefix'],     'required'],
                    [['catalog_items_per_page', 'list_description_limit'], 'number', 'integerOnly' => true],
                    [['invoice_prefix', 'catalog_items_per_page', 'list_description_limit', 'display_price_with_tax', 'tax_calculation_based_on', 
                      'guest_checkout', 'order_status', 'display_stock', 'customer_online', 'default_customer_group', 'allow_reviews', 
                      'allow_guest_reviews', 'show_out_of_stock_warning', 'allow_out_of_stock_checkout', 'allow_wishlist', 'allow_compare_products', 
                      'customer_prefix', 'order_prefix', 'display_weight', 'display_dimensions'],   'safe'],
               ];
	}

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenario = parent::scenarios();
        $scenario['create'] = $scenario['update'] = ['invoice_prefix', 'catalog_items_per_page', 'list_description_limit', 'display_price_with_tax', 
        'tax_calculation_based_on', 'guest_checkout', 'order_status', 'display_stock', 'customer_online', 'default_customer_group', 'allow_reviews', 
        'allow_guest_reviews', 'show_out_of_stock_warning', 'allow_out_of_stock_checkout', 'allow_wishlist', 'allow_compare_products', 
        'customer_prefix', 'order_prefix', 'display_weight', 'display_dimensions'];
        return $scenario;
    }
    
    /**
     * @inheritdoc
     */
	public function attributeLabels()
	{
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
		return [
                        'id'                            => UsniAdaptor::t('application','Id'),
                        'invoice_prefix'                => UsniAdaptor::t('stores',($language=='en-US')?'Invoice Prefix':'Prefijo de la factura'),
                        'catalog_items_per_page'        => UsniAdaptor::t('stores', ($language=='en-US')?'Number of items per page for catalog':'Número de artículos por página para el catálogo'),
                        'list_description_limit'        => UsniAdaptor::t('stores', ($language=='en-US')?'List Description  Limit':'Lista Descripción Límite'),
                        'display_price_with_tax'        => UsniAdaptor::t('stores', ($language=='en-US')?'Display Price With Tax':'Visualizar precio con impuestos'),
                        'tax_calculation_based_on'      => UsniAdaptor::t('stores', ($language=='en-US')?'Tax Calculation Based On':'Cálculo de impuestos basado en'),
                        'guest_checkout'                => UsniAdaptor::t('stores', ($language=='en-US')?'Guest Checkout':'Pago de Invitados'),
                        'order_status'                  => UsniAdaptor::t('orderstatus', ($language=='en-US')?'Order Status':'Estado del pedido'),
                        'display_stock'                 => UsniAdaptor::t('stores', ($language=='en-US')?'Display Stock':'Visualizar stock'),
                        'customer_online'               => UsniAdaptor::t('stores', ($language=='en-US')?'Customer Online':'Cliente en línea'),
                        'default_customer_group'        => UsniAdaptor::t('stores', ($language=='en-US')?'Default Customer Group':'Grupo de clientes por defecto'),
                        'allow_reviews'                 => UsniAdaptor::t('stores', ($language=='en-US')?'Allow Reviews':'Permitir revisiones'),
                        'allow_guest_reviews'           => UsniAdaptor::t('stores', ($language=='en-US')?'Allow Guest Reviews':'Permitir reseñas de invitados'),
                        'show_out_of_stock_warning'     => UsniAdaptor::t('stores', ($language=='en-US')?'Show out of stock warning':'Mostrar advertencia de falta de stock'),
                        'allow_out_of_stock_checkout'   => UsniAdaptor::t('stores', ($language=='en-US')?'Allow out of stock checkout':'Permitir la compra de productos fuera de stock'),
                        'allow_wishlist'                => UsniAdaptor::t('stores', ($language=='en-US')?'Allow Wishlist':'Permitir lista de deseos'),
                        'allow_compare_products'        => UsniAdaptor::t('stores', ($language=='en-US')?'Allow Compare Products':'Permitir Comparar Productos'),
                        'customer_prefix'               => UsniAdaptor::t('stores', ($language=='en-US')?'Customer Prefix':'Prefijo de cliente'),
                        'order_prefix'                  => UsniAdaptor::t('stores', ($language=='en-US')?'Order Prefix':'Prefijo de pedido'),
                        'display_weight'                => UsniAdaptor::t('stores', ($language=='en-US')?'Display Weight':'Mostrar peso'),
                        'display_dimensions'            => UsniAdaptor::t('stores', ($language=='en-US')?'Display Dimensions':'Dimensiones de la pantalla'),
                  ];
	}
    
    /**
     * @inheritdoc
     */
    public static function getLabel($n = 1)
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return UsniAdaptor::t('settings', ($language=='en-US')?'Settings':'Ajustes');
    }
    
    /**
     * @inheritdoc
     */
	public function attributeHints()
	{
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
		$hints = [
                        'invoice_prefix'            => UsniAdaptor::t('storehint',($language=='en-US')?'Set the invoice prefix (e.g. USNI). Invoice IDs will start at 1 for each unique prefix.':'Fije el prefijo de la factura (por ejemplo, USNI). Los ID de factura comenzarán a partir de 1 por cada prefijo único.'),
                        'catalog_items_per_page'    => UsniAdaptor::t('storehint', ($language=='en-US')?'Determines how many catalog items are shown per page (products, categories, etc).':'Determina cuántos artículos del catálogo se muestran por página (productos, categorías, etc.).'),
                        'list_description_limit'    => UsniAdaptor::t('storehint', ($language=='en-US')?'In the list view, short description character limit (categories, special etc).':'En la vista de lista, límite de caracteres de descripción breve (categorías, especiales, etc.).'),
                        'display_price_with_tax'    => UsniAdaptor::t('storehint', ($language=='en-US')?'Display Prices With Tax':'Visualizar precios con impuestos'),
                        'guest_checkout'            => UsniAdaptor::t('storehint', ($language=='en-US')?'Allow customers to checkout without creating an account. This will not be available when a downloadable product is in the shopping cart.':'Permitir a los clientes realizar el pago sin crear una cuenta. Esto no estará disponible cuando un producto descargable esté en el carrito de compras.'),
                        'order_status'              => UsniAdaptor::t('storehint', ($language=='en-US')?'Set the default order status when an order is processed.':'Fijar el status de la orden por defecto cuando se procesa una orden.'),
                        'display_stock'             => UsniAdaptor::t('storehint', ($language=='en-US')?'Display stock quantity on the product page.':'Mostrar la cantidad de existencias en la página del producto.'),
                        'customer_online'           => UsniAdaptor::t('storehint', ($language=='en-US')?'Track customers online via the customer reports section.':'Realice el seguimiento de los clientes en línea a través de la sección de informes de clientes.'),
                        'default_customer_group'    => UsniAdaptor::t('storehint', ($language=='en-US')?'Default Customer Group':'Grupo de clientes por defecto'),
                        'allow_reviews'             => UsniAdaptor::t('storehint', ($language=='en-US')?'Allow reviews on the product page':'Permitir comentarios en la página del producto'),
                        'allow_guest_reviews'       => UsniAdaptor::t('storehint', ($language=='en-US')?'Allow guests to post reviews.':'Permita que los invitados publiquen comentarios.'),
                        'show_out_of_stock_warning' => UsniAdaptor::t('storehint', ($language=='en-US')?'Show out of stock warning if item is not in stock':'Mostrar aviso de falta de stock si el artículo no está en stock'),
                        'allow_out_of_stock_checkout' => UsniAdaptor::t('storehint', ($language=='en-US')?'Allow checkout if item is not in stock':'Permitir el pago si el artículo no está en stock'),
                        'allow_wishlist'              => UsniAdaptor::t('storehint', ($language=='en-US')?'Allow store to display wishlist':'Permitir que la tienda muestre la lista de deseos'),
                        'allow_compare_products'      => UsniAdaptor::t('storehint', ($language=='en-US')?'Allow store to display compare products':'Permitir que la tienda muestre los productos comparados'),
                        'display_weight'              => UsniAdaptor::t('storehint', ($language=='en-US')?'Display weight of product.':'Visualización del peso del producto.'),
                        'display_dimensions'          => UsniAdaptor::t('storehint', ($language=='en-US')?'Display dimensions of product.':'Display dimensions of product.'),
                        'discount_amount_per_point'   => UsniAdaptor::t('storehint', ($language=='en-US')?'Discount amount customer will get per point.':'El cliente recibirá un descuento por punto.')
                  ];
        return $hints;
	}
}