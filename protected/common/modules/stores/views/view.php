<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
use usni\UsniAdaptor;
use usni\library\widgets\DetailActionToolbar;
use usni\fontawesome\FA;
use usni\library\widgets\Tabs;
use usni\library\widgets\DetailBrowseDropdown;

/* @var $detailViewDTO \usni\library\dto\DetailViewDTO */
/* @var $this \usni\library\web\AdminView */
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$model          = $detailViewDTO->getModel();
$this->title    = UsniAdaptor::t('application', ($language=='en-US')?'View':'Ver') . ' ' . UsniAdaptor::t('stores', ($language=='en-US')?'Store':'Tienda');
$this->params['breadcrumbs'] =  [
                                    [
                                        'label' => UsniAdaptor::t('application', ($language=='en-US')?'Manage':'Administrar') . ' ' .
                                        UsniAdaptor::t('stores', ($language=='en-US')?'Stores':'Tiendas'),
                                        'url' => ['/stores/default/index']
                                    ],
                                    [
                                        'label' => UsniAdaptor::t('application', ($language=='en-US')?'View':'Ver') . ' #' . $model['id']
                                    ]
                                ];

$browseParams   = ['permission' => 'stores.viewother',
                   'model' => $model,
                   'data'  => $detailViewDTO->getBrowseModels(),
                   'modalDisplay' => $detailViewDTO->getModalDisplay()];

echo DetailBrowseDropdown::widget($browseParams);
$toolbarParams  = ['editUrl'            => UsniAdaptor::createUrl('stores/default/update', ['id' => $model['id']]),
                   'deleteUrl'          => UsniAdaptor::createUrl('stores/default/delete', ['id' => $model['id']])];
?>
<div class="panel panel-default detail-container">
    <div class="panel-heading">
        <h6 class="panel-title"><?php echo FA::icon('book') . $model['name'];?></h6>
            <?php
                echo DetailActionToolbar::widget($toolbarParams);
            ?>
    </div>
    <?php
            $items[] = [
                'options' => ['id' => 'tabgeneral'],
                'label' => UsniAdaptor::t('application', ($language=='en-US')?'General':'General'),
                'class' => 'active',
                'content' => $this->render('/_storeview', ['detailViewDTO' => $detailViewDTO])
            ];
            $items[] = [
                'options' => ['id' => 'tablocal'],
                'label' => UsniAdaptor::t('stores', ($language=='en-US')?'Local':'Local'),
                'content' => $this->render('/_localview', ['detailViewDTO' => $detailViewDTO])
            ];
            $items[] = [
                'options' => ['id' => 'tabbillingaddress'],
                'label' => UsniAdaptor::t('application', ($language=='en-US')?'Billing Address':'Dirección de facturación'),
                'content' => $this->render('/_addressview', ['detailViewDTO' => $detailViewDTO, 'addressType' => 'billing'])
            ];
            $items[] = [
                'options' => ['id' => 'tabshippingaddress'],
                'label' => UsniAdaptor::t('application', ($language=='en-US')?'Shipping Address':'Dirección de envío'),
                'content' => $this->render('/_addressview', ['detailViewDTO' => $detailViewDTO, 'addressType' => 'shipping'])
            ];
            $items[] = [
                'options' => ['id' => 'tabsettings'],
                'label' => UsniAdaptor::t('stores', ($language=='en-US')?'Settings':'Ajustes'),
                'content' => $this->render('/_settingsview', ['detailViewDTO' => $detailViewDTO])
            ];
            $items[] = [
                'options' => ['id' => 'tabimage'],
                'label' => UsniAdaptor::t('stores', ($language=='en-US')?'Image':'Image'),
                'content' => $this->render('/_imageview', ['detailViewDTO' => $detailViewDTO])
            ];
            echo Tabs::widget(['items' => $items]);
    ?>
</div>