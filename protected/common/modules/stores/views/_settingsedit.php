<?php
use usni\UsniAdaptor;
use usni\library\utils\Html;
use usni\library\utils\AdminUtil;
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$catalog            = Html::tag('legend', UsniAdaptor::t('catalog', ($language=='en-US')?'Catalog':'Catálogo'));
$tax                = Html::tag('legend', UsniAdaptor::t('tax', ($language=='en-US')?'Taxes':'Impuestos'));
$account            = Html::tag('legend', UsniAdaptor::t('users', ($language=='en-US')?'Account':'Cuenta'));
$checkout           = Html::tag('legend', UsniAdaptor::t('cart', ($language=='en-US')?'Checkout':'Realizar Pedido'));
$stock              = Html::tag('legend', UsniAdaptor::t('products', ($language=='en-US')?'Stock':'Existencias'));
$reviews            = Html::tag('legend', UsniAdaptor::t('products', ($language=='en-US')?'Reviews':'Revisiones'));
$wishlist           = Html::tag('legend', UsniAdaptor::t('wishlist', ($language=='en-US')?'Wishlist':'Lista de deseos'));
$compareProducts    = Html::tag('legend', UsniAdaptor::t('products', ($language=='en-US')?'Compare Products':'Comparar productos'));
$local              = Html::tag('legend', UsniAdaptor::t('localization', ($language=='en-US')?'Local':'Local'));

/* @var $formDTO \common\modules\stores\dto\FormDTO */
/* @var $form \usni\library\bootstrap\ActiveForm */

$model = $formDTO->getModel()->storeSettings;
?>
<?= $catalog;?>
<?= $form->field($model, 'catalog_items_per_page')->textInput();?>
<?= $form->field($model, 'list_description_limit')->textInput();?>
<?= $tax;?>
<?= $form->field($model, 'display_price_with_tax')->dropDownList(AdminUtil::getYesNoOptions());?>
<?= $form->field($model, 'tax_calculation_based_on')->dropDownList($formDTO->getTaxBasedOnOptions());?>
<?= $account;?>
<?= $form->field($model, 'customer_online')->dropDownList(AdminUtil::getYesNoOptions());?>
<?= $form->field($model, 'default_customer_group')->select2input($formDTO->getCustomerGroupOptions());?>
<?= $form->field($model, 'customer_prefix')->textInput();?>
<?= $checkout;?>
<?= $form->field($model, 'invoice_prefix')->textInput();?>
<?= $form->field($model, 'order_prefix')->textInput();?>
<?= $form->field($model, 'guest_checkout')->dropDownList(AdminUtil::getYesNoOptions());?>
<?= $form->field($model, 'order_status')->select2input($formDTO->getOrderStatusOptions());?>
<?= $stock;?>
<?= $form->field($model, 'display_stock')->dropDownList(AdminUtil::getYesNoOptions());?>
<?= $form->field($model, 'show_out_of_stock_warning')->dropDownList(AdminUtil::getYesNoOptions());?>
<?= $form->field($model, 'allow_out_of_stock_checkout')->dropDownList(AdminUtil::getYesNoOptions());?>
<?= $reviews;?>
<?= $form->field($model, 'allow_reviews')->dropDownList(AdminUtil::getYesNoOptions());?>
<?= $form->field($model, 'allow_guest_reviews')->dropDownList(AdminUtil::getYesNoOptions());?>

<?= $wishlist;?>
<?= $form->field($model, 'allow_wishlist')->dropDownList(AdminUtil::getYesNoOptions());?>

<?= $compareProducts;?>
<?= $form->field($model, 'allow_compare_products')->dropDownList(AdminUtil::getYesNoOptions());?>

<?= $local;?>
<?= $form->field($model, 'display_dimensions')->dropDownList(AdminUtil::getYesNoOptions());?>
<?= $form->field($model, 'display_weight')->dropDownList(AdminUtil::getYesNoOptions());?>