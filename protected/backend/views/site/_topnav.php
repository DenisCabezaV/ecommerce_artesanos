<?php
/**
 * @copyright Copyright (C) 2016 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */

/* @var $this \usni\library\web\AdminView */

use usni\UsniAdaptor;
use usni\fontawesome\FA;
use usni\library\utils\Html;
use yii\helpers\Url;
use backend\widgets\LanguageSelector;
use backend\widgets\StoreSelector;
use usni\library\modules\settings\widgets\TopNavSettingsMenu;
use usni\library\modules\users\widgets\TopNavUserMenu;

$languageSelection = UsniAdaptor::app()->languageManager->selectedLanguage;

$label = ($languageSelection == 'es-EC') ? 'Tienda' : 'Store' ;
?>
<!--Navbar Begins-->
<div class="navbar navbar-inverse" role="navigation">
    <?= $this->render('@usni/library/views/_navbarHeader', ['title' => UsniAdaptor::app()->name . ' ' . UsniAdaptor::t('application', 'CPanel')]); ?>
    <ul class="nav navbar-nav navbar-right" id="navbar-icons">
    <?php if (!UsniAdaptor::app()->user->isGuest && UsniAdaptor::app()->isRebuildInProgress() === false) { ?>
        <?= StoreSelector::widget([
            'selectedStore' => UsniAdaptor::app()->storeManager->selectedStore,
            'stores' => UsniAdaptor::app()->storeManager->getAllowed()
        ]); ?>
        <?= LanguageSelector::widget([
            'selectedLanguage' => $languageSelection,
            'translatedLanguages' => UsniAdaptor::app()->languageManager->translatedLanguages,
            'languages' => UsniAdaptor::app()->languageManager->languages
        ]); ?>
        <li>
            <?php
            $url_to_store = str_replace('backend/', '', Url::toRoute('/site/default/index'));
            echo Html::a($label, $url_to_store); ?>
        </li>
        <!--<li>
            <?php
            $label = FA::icon('trash') . "\n" .
                ($languageSelection == 'es-EC' ? 'Limpiar Cache' :
                    UsniAdaptor::t('application', 'Clear Cache'));
            echo Html::a($label, Url::current(['clearCache' => 'true']));
            ?>
        </li>-->
        <?= TopNavSettingsMenu::widget(); ?>
        <?= TopNavUserMenu::widget(); ?>
    <?php } else { ?>
        <li>
            <?php
            $url_to_store = str_replace('backend/', '', Url::toRoute('/site/default/index'));
            echo Html::a($label, $url_to_store); ?>
        </li>
    <?php } ?>
    </ul>
</div>
<!--Navbar ends-->