<?php
/**
 * @copyright Copyright (c) 2017 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://github.com/ushainformatique/yiichimp/blob/master/LICENSE.md
 */
namespace usni\library\modules\users\utils;

use usni\UsniAdaptor;
use usni\library\modules\users\models\User;
/**
 * Contains utility functions related to Users.
 * 
 * @package usni\library\modules\users\utils
 */
class UserUtil
{
    /**
     * Get address labels
     * @return array
     */
    public static function getAddressLabels()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return [
            'address1'          => UsniAdaptor::t('users', ($language=='en-US')?'Address1':'Dirección1'),
            'address2'          => UsniAdaptor::t('users', ($language=='en-US')?'Address2':'Dirección2'),
            'city'              => UsniAdaptor::t('city', ($language=='en-US')?'City':'Ciudad'),
            'state'             => UsniAdaptor::t('state', ($language=='en-US')?'State':'Estado'),
            'country'           => UsniAdaptor::t('country', ($language=='en-US')?'Country':'País'),
            'postal_code'       => UsniAdaptor::t('users', ($language=='en-US')?'Postal Code':'Código Postal'),
            'status'            => UsniAdaptor::t('application', ($language=='en-US')?'Status':'Estado'),
            'relatedmodel'      => UsniAdaptor::t('users',($language=='en-US')?'Related Model':'Modelo Relacionado'),
            'relatedmodel_id'   => UsniAdaptor::t('users',($language=='en-US')?'Related Id':'Id Relacionado'),
            'useBillingAddress' => UsniAdaptor::t('users',($language=='en-US')?'Same As Billing Address':'La misma que la dirección de facturación'),
        ];
    }
    
    /**
     * Get person labels
     * @return array
     */
    public static function getPersonLabels()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return [
                    'id'                => UsniAdaptor::t('application', 'Id'),
                    'firstname'         => UsniAdaptor::t('users',($language=='en-US')?'First Names':'Nombres'),
                    'lastname'          => UsniAdaptor::t('users',($language=='en-US')?'Last Names':'Apellidos'),
                    'mobilephone'       => UsniAdaptor::t('users',($language=='en-US')?'Mobiles':'Teléfono móvil'),
                    'email'             => UsniAdaptor::t('users',($language=='en-US')?'Email':'E-mail'),
                    'fullName'          => UsniAdaptor::t('users',($language=='en-US')?'Full Name':'Nombre y Apellidos'),
                    'profile_image'     => UsniAdaptor::t('users',($language=='en-US')?'Profile Image':'Imagen de perfil')
                ];
    }
    
    /**
     * Get user labels
     * @return array
     */
    public static function getUserLabels()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return [
            'username'          => UsniAdaptor::t('users', ($language=='en-US')?'Username':'Nombre de usuario'),
            'password'          => UsniAdaptor::t('users', ($language=='en-US')?'Password':'Contraseña'),
            'confirmPassword'   => UsniAdaptor::t('users', ($language=='en-US')?'Confirm Password':'Confirmar Contraseña'),
            'timezone'          => UsniAdaptor::t('users', ($language=='en-US')?'Timezone':'Zona horaria'),
            'status'            => UsniAdaptor::t('application', ($language=='en-US')?'Status':'Estado'),
            'groups'            => UsniAdaptor::t('auth',  ($language=='en-US')?'Group':'Grupo')
        ];
    }
    
    /**
     * Get password instructions.
     * @return string
     */
    public static function getPasswordInstructions()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return UsniAdaptor::t('userflash', ($language=='en-US')?'<div class="notifications">
                                                    <ul>
                                                        <li>Must contains one digit from 0-9</li>
                                                        <li>Must contains one alphabet(case insensitive)</li>
                                                        <li>Must contains one special symbol</li>
                                                        <li>Match anything with previous condition checking
                                                                {6,20} length at least 6 characters and maximum of 20</li>
                                                    </ul>
                                                </div>': '<div class="notifications">
                                                    <ul>
                                                        <li>Debe contiene un dígito de 0-9</li>
                                                        <li>Debe contener un alfabeto (insensible a mayúsculas y minúsculas)</li>
                                                        <li>Debe contener un símbolo especial</li>
                                                        <li>Coincidir con cualquier cosa con condición previa comprobando {6,20} longitud de al menos 6 caracteres y un máximo de 20</li>
                                                    </ul>
                                                </div>');
    }
    
    /**
     * Generate random password.
     * @return string.
     */
    public static function generateRandomPassword()
    {
        $chars      = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $password   = substr( str_shuffle( $chars ), 0, 8);
        return $password;
    }
    
    /**
     * Generate special character.
     * @return string.
     */
    public static function generateSpecialChar()
    {
        $chars      = "!@#$%^&*";
        $chosenChar   = substr( str_shuffle( $chars ), 0, 1);
        return $chosenChar;
    }
    
     /**
     * Gets status drop down.
     * @return array
     */
    public static function getStatusDropdown()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return [
                    User::STATUS_ACTIVE     => UsniAdaptor::t('application',($language=='en-US')?'Active':'Activo'),
                    User::STATUS_INACTIVE   => UsniAdaptor::t('application',($language=='en-US')?'Inactive':'Inactivo'),
                    User::STATUS_PENDING    => UsniAdaptor::t('application',($language=='en-US')?'Pending':'Pendiente')
               ];
    }
}