<?php
/**
 * @copyright Copyright (c) 2017 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://github.com/ushainformatique/yiichimp/blob/master/LICENSE.md
 */
namespace usni\library\modules\users\models;

use usni\UsniAdaptor;
/**
 * SettingsForm class file.
 * 
 * @package usni\library\modules\users\models
 */
class SettingsForm extends \yii\base\Model
{
    /**
     * Duration for password token expiry.
     * @var int
     */
    public $passwordTokenExpiry;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
                    [['passwordTokenExpiry'],   'number', 'integerOnly' => true],
                    [['passwordTokenExpiry'],   'required'],
               ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return [
                    'passwordTokenExpiry' => UsniAdaptor::t('users', ($language=='en-US')?'Password token expiry':'Expiración del token de la contraseña'),
               ];
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        $language = UsniAdaptor::app()->languageManager->selectedLanguage;
        return [
                    'passwordTokenExpiry' => UsniAdaptor::t('userhint', ($language=='en-US')?'Duration after which password token expire in seconds':'Duración después de la cual el token de la contraseña expira en segundos'),
               ];
    }
}