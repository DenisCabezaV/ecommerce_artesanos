<?php
/**
 * @copyright Copyright (c) 2017 Usha Singhai Neo Informatique Pvt. Ltd
 * @license https://github.com/ushainformatique/yiichimp/blob/master/LICENSE.md
 */
use usni\UsniAdaptor;
use usni\library\grid\GridView;
use usni\library\modules\users\models\User;
use usni\library\modules\users\widgets\UserNameDataColumn;
use usni\library\grid\StatusDataColumn;

/* @var $gridViewDTO \usni\library\dto\GridViewDTO */
/* @var $this \usni\library\web\AdminView */
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
$title          = UsniAdaptor::t('users', ($language=='en-US')?'Latest Users':'Últimos usuarios');

$widgetParams   = [
                        'id'            => 'latestusersgridview',
                        'dataProvider'  => $gridViewDTO->getDataProvider(),
                        'caption'       => $title,
                        'modelClass'    => User::className(),
                        'layout'        => "<div class='panel panel-default'>
                                                <div class='panel-heading'>{caption}</div>
                                                    {items}
                                               </div>",
                        'columns'       => [
                                                [
                                                    'label' => ($language=='en-US')?'Username':'Usuario',
                                                    'attribute' => 'username',
                                                    'class'     => UserNameDataColumn::className()
                                                ],
                                                [
                                                    'label' => ($language=='en-US')?'Email':'E-mail',
                                                    'attribute' =>'email',
                                                ],
                                                [
                                                    'label' => ($language=='en-US')?'Status':'Estado',
                                                    'attribute' => 'status',
                                                    'class' => StatusDataColumn::className()
                                                ],
                                        ],
                ];
echo GridView::widget($widgetParams);