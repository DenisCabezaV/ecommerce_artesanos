<?php
use usni\library\utils\MenuUtil;
use usni\UsniAdaptor;
$language = UsniAdaptor::app()->languageManager->selectedLanguage;
if(UsniAdaptor::app()->user->can('access.users'))
{
    return [
                [
                    'label'       => MenuUtil::wrapLabel(UsniAdaptor::t('users', ($language=='en-US')?'Users':'Usuarios')),
                    'url'         => ['/users/default/index'],
                    'itemOptions' => ['class' => 'navblock-header']
                ]
            ];
}
return [];

